export const environment = {
  production: true,
  apiURL: 'https://jeblo-back-stage.herokuapp.com',
  firebaseConfig: {
    apiKey: "AIzaSyB1Ewirn0oL8PhupZ9MjS_c5EYylXJJLvg",
    authDomain: "jeblo-storage.firebaseapp.com",
    databaseURL: "https://jeblo-storage.firebaseio.com",
    projectId: "jeblo-storage",
    storageBucket: "jeblo-storage.appspot.com",
    messagingSenderId: "180059429737",
    appId: "1:180059429737:web:d30c0a0b0aba3c3eb7fbf8"
  }
};
