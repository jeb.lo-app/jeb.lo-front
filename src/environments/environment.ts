// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiURL: 'http://localhost:8080',
  firebaseConfig: {
    apiKey: 'AIzaSyB1Ewirn0oL8PhupZ9MjS_c5EYylXJJLvg',
    authDomain: 'jeblo-storage.firebaseapp.com',
    databaseURL: 'https://jeblo-storage.firebaseio.com',
    projectId: 'jeblo-storage',
    storageBucket: 'jeblo-storage.appspot.com',
    messagingSenderId: '180059429737',
    appId: '1:180059429737:web:d30c0a0b0aba3c3eb7fbf8'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error';  // Included with Angular CLI.
