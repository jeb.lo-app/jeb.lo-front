// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.15.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  apiKey: 'AIzaSyB1Ewirn0oL8PhupZ9MjS_c5EYylXJJLvg',
  authDomain: 'jeblo-storage.firebaseapp.com',
  databaseURL: 'https://jeblo-storage.firebaseio.com',
  projectId: 'jeblo-storage',
  storageBucket: 'jeblo-storage.appspot.com',
  messagingSenderId: '180059429737',
  appId: '1:180059429737:web:d30c0a0b0aba3c3eb7fbf8'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
  // Customize notification here
  const notificationTitle = payload.data.content;
  const notificationOptions = {
    body: payload.data.body,
    icon: 'assets/notification-icon.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});
