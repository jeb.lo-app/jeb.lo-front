import {Component, OnInit} from '@angular/core';
import {Post} from '../../models/post/Post';
import {PostsService} from '../../services/api/posts/posts.service';
import {SnackbarService} from '../../services/snackbar/snackbar.service';
import * as Leaflet from 'leaflet';
import {Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-events-map',
  templateUrl: './events-map.component.html',
  styleUrls: ['./events-map.component.scss']
})
export class EventsMapComponent implements OnInit {
  posts: Post[];
  map: any;

  constructor(private postsService: PostsService,
              private snackbar: SnackbarService,
              private domSanitizer: DomSanitizer,
              private router: Router) {
  }

  ngOnInit() {
    this.postsService.getAllEvents(null,  ['NOWE', 'AKTUALNE']).subscribe({
      next: response => {
        this.posts = response.body;
        this.initMap();
      },
      error: _ => {
        this.snackbar.openErrorSnackbar('Nie udało się załadować postów');
        this.posts = [];
      }
    });

  }

  initMap() {
    console.log(this.posts);
    this.map = Leaflet.map('map-div', {
      center: [51.110777355, 17.0349669456],
      zoom: 13,
      keyboard: false,
      scrollWheelZoom: true,
      touchZoom: true,
      dragging: true,
      zoomControl: true
    });

    const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    tiles.addTo(this.map);


    for (const p of this.posts) {

      let lines = '';
      for (const l of p.linesBlocked) {
        lines += l + ', ';
      }
      lines = lines.substring(0, lines.length - 2);
      let customPopup =
        `<a href='/event/${p.id}' style='text-decoration:none; color: black;'>` +
        `    <div style='text-align:right;font-size:10px;'>` +
        `         ${p.postedDate.toLocaleString()}` +
        `    </div>` +
        `    <br/>` +
        `    <div style='font-size:20px' class='text-oneline'>` +
        `        <b>${p.title}</b>` +
        `    </div>` +
        `    <br/>` +
        `    <div>` +
        `        Linie: ${lines}` +
        `        <div>` +
        `            Autor: <b>${p.authorUsername}</b>` +
        `        </div>` +
        `    </div>`;
      if (p.photoSourceURLs.length > 0) {
        customPopup += `    <img width="250px" mat-card-image src='${p.photoSourceURLs[0]}'>`;
      }

      customPopup += `  </mat-card>` +
        `</div> </a>`;

      const customOptions = {
        maxWidth: '250',
        className: 'custom',
        closeButton: false,
        autoClose: false
      };

      Leaflet.marker(p.coordinates, {interactive: true})
        .addTo(this.map)
        .bindPopup(customPopup, customOptions);
      // .bindPopup(`${p.title} ${p.postedDate.toLocaleString()}`);
    }

  }

  navigateToEvent(id) {
    this.router.navigateByUrl(`event/${id}`);
  }

}
