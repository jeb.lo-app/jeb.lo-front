import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {Router} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {EventsComponent} from '../events/events.component';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {AuthService} from '../../services/api/auth/auth.service';
import {FormsModule} from '@angular/forms';
import {of, throwError} from 'rxjs';
import {By} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        FormsModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(
          [{path: 'events', component: EventsComponent}]
        ),
      ],
      declarations: [ LoginComponent ]
    })
    .compileComponents();
  }));

  it('should create', inject([TokenStorageService], (tokenStorageService: TokenStorageService) => {
    spyOn(tokenStorageService, 'userIsLoggedIn').and.returnValue(false);

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  }));

  it('should create and redirect logged user',
    inject([Router, TokenStorageService],
      (router: Router, tokenStorageService: TokenStorageService) => {
    spyOn(router, 'navigateByUrl').and.returnValue(Promise.resolve(true));
    spyOn(tokenStorageService, 'userIsLoggedIn').and.returnValue(true);

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/events');
  }));

  it('should login user', inject([Router, TokenStorageService, AuthService],
    (router: Router, tokenStorageService: TokenStorageService, authService: AuthService) => {
      const username = 'test';
      const password = 'loong-password';
      const token = 'long-token';
      spyOn(router, 'navigateByUrl').and.returnValue(Promise.resolve(true));
      spyOn(tokenStorageService, 'saveToken').and.stub();
      spyOn(tokenStorageService, 'saveUser').and.stub();
      spyOn(authService, 'login').and.returnValue(of({token}));

      fixture = TestBed.createComponent(LoginComponent);
      component = fixture.componentInstance;
      component.username = username;
      component.password = password;
      component.reloadPage = () => {};
      fixture.detectChanges();

      expect(component).toBeTruthy();
      fixture.debugElement.query(By.css('#login-btn')).triggerEventHandler('click', null);
      expect(authService.login).toHaveBeenCalledWith({username, password});
      expect(tokenStorageService.saveToken).toHaveBeenCalledWith(token);
      expect(router.navigateByUrl).toHaveBeenCalledWith('/events');
  }));

  it('should fail to login user - unauthorized', inject([AuthService],
    (authService: AuthService) => {
      const username = 'test';
      const password = 'loong-password';
      spyOn(authService, 'login').and.returnValue(throwError({error: {message: 'Error: Unauthorized'}}));
      fixture = TestBed.createComponent(LoginComponent);
      component = fixture.componentInstance;
      component.username = username;
      component.password = password;
      fixture.detectChanges();

      fixture.debugElement.query(By.css('#login-btn')).triggerEventHandler('click', null);
      expect(authService.login).toHaveBeenCalledWith({username, password});
    }));

  it('should fail to login user', inject([AuthService],
    (authService: AuthService) => {
      spyOn(authService, 'login').and.returnValue(throwError({error: {message: 'Error'}}));
      const username = 'test';
      const password = 'loong-password';
      fixture = TestBed.createComponent(LoginComponent);
      component = fixture.componentInstance;
      component.username = username;
      component.password = password;
      fixture.detectChanges();

      fixture.debugElement.query(By.css('#login-btn')).triggerEventHandler('click', null);
      expect(authService.login).toHaveBeenCalledWith({username, password});
    }));
});
