import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/api/auth/auth.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {SnackbarService} from '../../services/snackbar/snackbar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              private authService: AuthService,
              private tokenStorage: TokenStorageService,
              private snackbar: SnackbarService) {
  }
  username: string;
  password: string;
  errorMessage: any;
  isLoginFailed = false;
  awaitingServerResponse = false;

  reloadPage() {
    window.location.reload();
  }

  ngOnInit() {
    if (this.tokenStorage.userIsLoggedIn()) {
      this.router.navigateByUrl('/events').then(_ => this.snackbar.openSuccessSnackbar(`Witaj, ${this.username}!`));
    }
  }

  login() {
    this.awaitingServerResponse = true;
    this.authService.login({username: this.username, password: this.password}).subscribe(
      data => {
        this.awaitingServerResponse = false;
        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveUser(data);
        this.isLoginFailed = false;


        this.router.navigateByUrl('/events').then(_ => {
          this.reloadPage();
        });
      },
      err => {
        this.awaitingServerResponse = false;
        if (err.error.message === 'Error: Unauthorized') {
          this.errorMessage = 'Nieprawidłowy login lub hasło.';
        } else {
          this.errorMessage = 'Błąd podczas logowania.';
        }
        this.isLoginFailed = true;
      }
    );
  }
}
