import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {map, startWith} from 'rxjs/operators';

/**
 * Adapted from https://material.angular.io/components/chips/overview#chip-input
 * IMPORTANT: this component doesn't exactly add or remove any elements; it just sends 'added' and 'removed' events
 * but actual operations have to be handled by the parent component.
 */
@Component({
  selector: 'app-chip-autocomplete-input',
  templateUrl: './chip-autocomplete-input.component.html',
  styleUrls: ['./chip-autocomplete-input.component.scss']
})
export class ChipAutocompleteInputComponent implements OnInit {

  @Input()
  required = false;

  @Input()
  label: string;

  removable = true;
  separatorKeysCodes: number[] = [ENTER];
  valueControl: FormControl;
  autocompleteValues: Observable<string[]>;

  initialized = false;
  chosenValues: string[] = [];
  allValues: string[] = [];

  @Output()
  chosenValuesChanged: EventEmitter<string[]> = new EventEmitter<string[]>();

  @Output()
  singleValueChanged: EventEmitter<{ value: string, operationType: 'add' | 'delete' }> =
    new EventEmitter<{value: string, operationType: 'add'|'delete'}>();

  @Output()
  valid: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild('valueInput') valueInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor() {
    this.valueControl = new FormControl('');

    // this sets which values are displayed in autocomplete options
    this.autocompleteValues = this.valueControl.valueChanges.pipe(
      startWith(null as string),
      map((value: string) => this._filter(value))
    );
  }

  ngOnInit(): void {

  }

  // this is meant to be used ONCE to initialize the values in the list;
  public initChosenAndAllValues(chosenValues: string[], allValues: string[]) {

    if (!this.initialized) {
      this.initialized = true;
      chosenValues.forEach(value => this.chosenValues.push(value));
      allValues.forEach(value => this.allValues.push(value));

      // forces autocomplete to change
      this.valueControl.setValue(null);
    } else {
      console.warn('input already initialized with chosen and all values!');
    }
  }

  addValue(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if (this.allValues.includes(value) && !this.chosenValues.includes(value)) {
      this.chosenValues.push(value);
      this.singleValueChanged.emit({value, operationType: 'add'});
      this.chosenValuesChanged.emit(this.chosenValues);
      this.valid.emit(this.required ? (this.chosenValues.length === 0) : true);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.valueControl.setValue(null);

  }

  removeValue(value: string): void {
    const index = this.chosenValues.indexOf(value);
    this.chosenValues.splice(index, 1);
    this.chosenValuesChanged.emit(this.chosenValues);
    this.singleValueChanged.emit({value, operationType: 'delete'});
    this.valid.emit(this.required ? (this.chosenValues.length === 0) : true);
    this.valueControl.setValue(null);
  }

  valueSelectedFromAutoComplete(event: MatAutocompleteSelectedEvent): void {
    this.chosenValues.push(event.option.viewValue);
    this.singleValueChanged.emit({value: event.option.viewValue, operationType: 'add'});
    this.chosenValuesChanged.emit(this.chosenValues);
    this.valid.emit(this.required ? (this.chosenValues.length === 0) : true);

    this.valueInput.nativeElement.value = '';
    this.valueInput.nativeElement.blur();
    this.valueControl.setValue(null);
  }

  public setInputDisabled(disabled: boolean) {
    if (disabled) {
      this.chosenValues = [];
      this.valueControl.disable();
    } else {
      this.valueControl.enable();
    }
    this.valueInput.nativeElement.disabled = disabled;
  }

  public get isInitialized(): boolean {
    return this.initialized;
  }

  // this is the autocomplete filter; returns array of strings that begin with input value
  private _filter(inputValue: string): string[] {
    return this.allValues
      .filter(value => {
        return (!!inputValue ? value.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0 : true) &&
        !this.chosenValues.includes(value);
      })
      .sort((a, b) => {
        if (inputValue == null) {
          inputValue = '';
        }

        if (a.toLowerCase().startsWith(inputValue.toLowerCase())) {
          if (b.toLowerCase().startsWith(inputValue.toLowerCase())) {
            return a.localeCompare(b, 'pl', {sensitivity: 'base'});
          } else {
            return -1;
          }
        } else {
          if (b.toLowerCase().startsWith(inputValue.toLowerCase())) {
            return 1;
          } else {
            return a.localeCompare(b, 'pl', {sensitivity: 'base'});
          }
        }
      });
  }
}
