import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultValue',
  pure: false
})
export class DefaultValuePipe implements PipeTransform {

  transform(value: unknown, defaultValue: any): unknown {
    return value === undefined ? defaultValue : value;
  }

}
