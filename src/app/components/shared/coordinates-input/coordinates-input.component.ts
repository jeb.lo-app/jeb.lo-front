import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import * as Leaflet from 'leaflet';

@Component({
  selector: 'app-coordinates-input',
  templateUrl: './coordinates-input.component.html',
  styleUrls: ['./coordinates-input.component.scss']
})
export class CoordinatesInputComponent implements OnInit {
  private defaultCoordinates = [51.110777355, 17.0349669456];
  map: any;
  locationMarker: any;

  @Output() coordinates = new EventEmitter<{lat: number, lng: number}>();

  constructor() { }

  ngOnInit(): void {
  }

  initMap(startingMarker?: any) {
    if (this.map !== undefined) {
      return;
    }
    this.map = Leaflet.map('map', {center: this.defaultCoordinates, zoom: 13});

    const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    tiles.addTo(this.map);

    if (startingMarker) {
      this.locationMarker = Leaflet.marker(startingMarker).addTo(this.map);
      this.map.setView(startingMarker, 17);
    } else {
      this.map.locate({setView: true});
    }
    this.map.on('locationfound', event => {
      this.locationMarker = Leaflet.marker(event.latlng).addTo(this.map);
      this.coordinates.emit(event.latlng);
    });
    this.map.on('click', event => {
      if (this.locationMarker === undefined) {
        this.locationMarker = Leaflet.marker(event.latlng).addTo(this.map);
      } else {
        this.locationMarker.setLatLng(event.latlng);
      }
      this.coordinates.emit(event.latlng);
    });
  }

}
