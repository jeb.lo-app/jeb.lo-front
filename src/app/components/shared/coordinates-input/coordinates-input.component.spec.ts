import {async, ComponentFixture, fakeAsync, flush, TestBed, tick} from '@angular/core/testing';

import { CoordinatesInputComponent } from './coordinates-input.component';
import * as Leaflet from 'leaflet';

describe('CoordinatesInputComponent', () => {
  let component: CoordinatesInputComponent;
  let fixture: ComponentFixture<CoordinatesInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoordinatesInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoordinatesInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not init map', () => {
    spyOn(component, 'initMap').and.callThrough();
    component.map = {};
    fixture.detectChanges();
    component.initMap();
    expect(component.initMap).toHaveBeenCalled();
  });

  it('should init map', () => {
    spyOn(component, 'initMap').and.callThrough();
    fixture.detectChanges();
    component.initMap();
    expect(component.initMap).toHaveBeenCalled();
  });

  it('should init map with starterMarker', () => {
    spyOn(component, 'initMap').and.callThrough();
    fixture.detectChanges();
    const marker = {lat: 10, lng: 10};
    component.initMap(marker);
    expect(component.initMap).toHaveBeenCalledWith(marker);
    expect(component.locationMarker.getLatLng()).toEqual(Leaflet.marker(marker).getLatLng());
  });

  it('should add marker on click', fakeAsync(() => {
    const marker = {lat: 10, lng: 10};
    const point = new Leaflet.LatLng(10, 10);

    spyOn(component, 'initMap').and.callThrough();
    spyOn(component.coordinates, 'emit');
    fixture.detectChanges();

    component.initMap();
    component.map.fireEvent('click', {
      latlng: point,
      layerPoint: component.map.latLngToLayerPoint(point),
      containerPoint: component.map.latLngToContainerPoint(point)
    });
    expect(component.locationMarker.getLatLng()).toEqual(Leaflet.marker(marker).getLatLng());
    expect(component.coordinates.emit).toHaveBeenCalledWith(point);
  }));

  it('should add marker on click with starting marker', fakeAsync(() => {
    const marker = {lat: 10, lng: 10};
    const point = new Leaflet.LatLng(10, 10);

    spyOn(component, 'initMap').and.callThrough();
    spyOn(component.coordinates, 'emit');
    fixture.detectChanges();

    component.initMap(marker);
    component.map.fireEvent('click', {
      latlng: point,
      layerPoint: component.map.latLngToLayerPoint(point),
      containerPoint: component.map.latLngToContainerPoint(point)
    });
    tick();
    expect(component.locationMarker.getLatLng()).toEqual(Leaflet.marker(marker).getLatLng());
    expect(component.coordinates.emit).toHaveBeenCalledWith(point);
    flush();
  }));

  it('should found location', fakeAsync(() => {
    const marker = {lat: 10, lng: 10};
    const point = new Leaflet.LatLng(10, 10);

    spyOn(component, 'initMap').and.callThrough();
    spyOn(component.coordinates, 'emit');
    fixture.detectChanges();

    component.initMap();
    component.map.fireEvent('locationfound', {
      latlng: point,
      layerPoint: component.map.latLngToLayerPoint(point),
      containerPoint: component.map.latLngToContainerPoint(point)
    });
    tick();
    expect(component.locationMarker.getLatLng()).toEqual(Leaflet.marker(marker).getLatLng());
    expect(component.coordinates.emit).toHaveBeenCalledWith(point);
  }));
});
