import { Component, OnInit } from '@angular/core';
import {ReportRequest} from '../../../models/post/ReportRequest';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-event-report-modal',
  templateUrl: './event-report-dialog.component.html',
  styleUrls: ['./event-report-dialog.component.scss']
})
export class EventReportDialogComponent implements OnInit {

  reportForm = new FormGroup({
    reason: new FormControl('', Validators.required),
    content: new FormControl('')
  });

  get reason() {
    return this.reportForm.get('reason');
  }

  get content() {
    return this.reportForm.get('content');
  }

  constructor() { }

  ngOnInit() {
  }

}
