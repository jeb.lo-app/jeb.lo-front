import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DisplayMapComponent} from './display-map/display-map.component';
import {ChipAutocompleteInputComponent} from './chip-autocomplete-input/chip-autocomplete-input.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {LoadingSpinnerComponent} from './loading-spinner/loading-spinner.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {IconSnackbarComponent} from './icon-snackbar/icon-snackbar.component';
import {CoordinatesInputComponent} from './coordinates-input/coordinates-input.component';
import {DefaultValuePipe} from './default-value/default-value.pipe';
import {SimpleInfoDialogComponent} from './simple-info-dialog/simple-info-dialog.component';
import {EventReportDialogComponent} from './event-report-dialog/event-report-dialog.component';
import {EventDeleteDialogComponent} from './event-delete-dialog/event-delete-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {PhotoUploaderComponent} from './photo-uploader/photo-uploader.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {FileUploadComponent} from './file-upload/file-upload.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';

@NgModule({
  declarations: [DisplayMapComponent,
    ChipAutocompleteInputComponent,
    LoadingSpinnerComponent,
    IconSnackbarComponent,
    DefaultValuePipe,
    CoordinatesInputComponent,
    SimpleInfoDialogComponent,
    EventReportDialogComponent,
    EventDeleteDialogComponent,
    PhotoUploaderComponent,
    FileUploadComponent],
  exports: [
    DisplayMapComponent,
    ChipAutocompleteInputComponent,
    LoadingSpinnerComponent,
    CoordinatesInputComponent,
    DefaultValuePipe,
    SimpleInfoDialogComponent,
    EventReportDialogComponent,
    EventDeleteDialogComponent,
    PhotoUploaderComponent,
    FileUploadComponent
  ],
    imports: [
        CommonModule,
        MatFormFieldModule,
        MatChipsModule,
        MatIconModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatSelectModule,
        MatButtonModule,
        MatInputModule,
        FormsModule,
        DragDropModule,
        MatProgressBarModule
    ]
})
export class SharedModule { }
