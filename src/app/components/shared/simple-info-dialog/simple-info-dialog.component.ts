import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-simple-info-dialog',
  templateUrl: './simple-info-dialog.component.html',
  styleUrls: ['./simple-info-dialog.component.css']
})
export class SimpleInfoDialogComponent implements OnInit {

  title: string;
  description: string;
  reloadAfterClose: string;

  constructor(
    private dialogRef: MatDialogRef<SimpleInfoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.description = data.desc;
    this.title = data.title;
    this.reloadAfterClose = data.reloadAfterClose;
  }

  ngOnInit(): void {
  }

}
