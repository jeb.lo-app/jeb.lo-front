import {Component, Input, OnInit} from '@angular/core';
import * as Leaflet from 'leaflet';

// as instructed by https://alligator.io/angular/angular-and-leaflet-marker-service/
// needed to display the marker properly
// we've PROBABLY configured the marker icons to be copied from node modules to application assets
// look at angular.json (architect/build/assets)
const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = Leaflet.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
Leaflet.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-display-map',
  templateUrl: './display-map.component.html',
  styleUrls: ['./display-map.component.scss']
})
export class DisplayMapComponent implements OnInit {

  private map: any;
  private marker: any;
  @Input() coordinates: Leaflet.LatLng;
  @Input() zoom = 15;

  constructor() { }

  ngOnInit(): void {
  }

  public updateMap(): void {
    if (this.map === undefined) {
      this.map = Leaflet.map('map-div', {
        center: this.coordinates,
        zoom: this.zoom,
        keyboard: false,
        scrollWheelZoom: true,
        touchZoom: true,
        dragging: true,
        zoomControl: true
      });

      const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      });
      tiles.addTo(this.map);

      this.marker = Leaflet.marker(this.coordinates, {
        interactive: false
      });
      this.marker.addTo(this.map);
    } else {
      this.map.setView(this.coordinates, this.zoom);
      this.marker.setLatLng(this.coordinates);
    }
  }
}
