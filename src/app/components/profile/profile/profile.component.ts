import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../models/user/User';
import {UsersService} from '../../../services/api/users/users.service';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {SnackbarService} from '../../../services/snackbar/snackbar.service';

@Component({
  selector: 'app-user',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userData: User;

  constructor(private activatedRoute: ActivatedRoute,
              private tokenStorage: TokenStorageService,
              private userService: UsersService,
              private router: Router,
              private snackbar: SnackbarService) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      if (params.get('username') === 'me' && !this.tokenStorage.userIsLoggedIn()) {
        this.router.navigateByUrl('/events').then(() => this.snackbar.openErrorSnackbar('Nie jesteś zalogowany!'));
      }
      this.userService
        .getUserDataByUsername(params.get('username'))
        .subscribe(response => {
          this.userData = response.body;
        });
    });
  }

  shownUserIsLoggedIn(): boolean {
    return this.userData !== undefined && this.userData.username === this.tokenStorage.getUsername();
  }

}
