import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NewEventComponent} from './new-event.component';
import {MaterialModule} from '../../material/material.module';
import {MatStepperModule} from '@angular/material/stepper';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {MatCarouselModule} from '@ngmodule/material-carousel';

@NgModule({
  declarations: [
    NewEventComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MatStepperModule,
    ReactiveFormsModule,
    SharedModule,
    MatCarouselModule
  ],
  exports: [
    NewEventComponent
  ]
})
export class NewEventModule { }
