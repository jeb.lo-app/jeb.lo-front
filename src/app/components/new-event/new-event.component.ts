import {Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CoordinatesInputComponent} from '../shared/coordinates-input/coordinates-input.component';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {DisplayMapComponent} from '../shared/display-map/display-map.component';
import {ChipAutocompleteInputComponent} from '../shared/chip-autocomplete-input/chip-autocomplete-input.component';
import {LinesService} from '../../services/api/lines/lines.service';
import {PostsService} from '../../services/api/posts/posts.service';
import {PostAddRequest} from '../../models/post/Post';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {SnackbarService} from '../../services/snackbar/snackbar.service';
import {PhotoUploaderComponent} from '../shared/photo-uploader/photo-uploader.component';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';

@Component({
  selector: 'app-new-event',
  templateUrl: './new-event.component.html',
  styleUrls: ['./new-event.component.scss']
})

export class NewEventComponent implements OnInit, OnDestroy {
  postToAdd: PostAddRequest;
  showSpinner = false;

  private isSubmitted = false;

  @ViewChild(CoordinatesInputComponent)
  private coordinatesInput: CoordinatesInputComponent;

  @ViewChild(DisplayMapComponent)
  private displayMap: DisplayMapComponent;

  @ViewChild(PhotoUploaderComponent)
  photoUploader: PhotoUploaderComponent;

  @ViewChild('lineInput')
  lineInput: ChipAutocompleteInputComponent;

  formGroup: FormGroup;

  lineInputIsValid = false;

  get formArray(): AbstractControl | null {
    return this.formGroup.get('formArray');
  }

  constructor(private formBuilder: FormBuilder,
              private linesService: LinesService,
              private postsService: PostsService,
              private dialog: MatDialog,
              private router: Router,
              private snackbar: SnackbarService,
              private tokenStorage: TokenStorageService) {
  }

  // Lines
  allLines: string[];

  ngOnInit() {
    if (!this.tokenStorage.userIsLoggedIn()) {
      return this.router.navigateByUrl('/events').then(() => this.snackbar.openErrorSnackbar('Nie jesteś zalogowany!'));
    }

    this.formGroup = this.formBuilder.group({
      formArray: this.formBuilder.array([
        this.formBuilder.group({
          titleFormCtrl: ['', [Validators.required, Validators.maxLength(70)]],
          lineFormCtrl: [[], Validators.required],
          descFormCtrl: ['', [Validators.required, Validators.maxLength(256)]]
        }),
        this.formBuilder.group({
          latFormCtrl: ['', Validators.required],
          lonFormCtrl: ['', Validators.required]
        }),
        this.formBuilder.group({
          photoURLFormCtrl: [[]]
        })
      ])
    });

    this.linesService.getAllLines().subscribe(response => {
      this.allLines = [...new Set(response.body.map(line => line.lineName))];
      this.lineInput.initChosenAndAllValues([], this.allLines);
    });

  }

  ngOnDestroy(): void {
    this.cleanIfNotSubmitted();
  }

  @HostListener('window:beforeunload')
  private cleanIfNotSubmitted() {
    if (!this.isSubmitted) {
      this.photoUploader.deletePhotos();
    }
  }

  onSelectionChanged(event: StepperSelectionEvent) {
    if (event.selectedIndex === 1) {
      this.coordinatesInput.initMap();
    } else if (event.selectedIndex === 3) {
      this.displayMap.updateMap();
    }
  }

  updateCoordinates(coordinates: { lat: number, lng: number }) {
    this.formArray.get([1]).setValue({
      latFormCtrl: coordinates.lat,
      lonFormCtrl: coordinates.lng
    });
  }

  updatePhotosToSubmit(photosList: string[]) {
    this.formArray.get([2]).get('photoURLFormCtrl').setValue(photosList);
  }

  updateLines(updatedLines: string[]) {
    // HACK: in order to prevent the buttons from going to the next step, invisible form control is being updated
    this.formArray.get([0]).get('lineFormCtrl').setValue(updatedLines.length > 0 ? updatedLines : null);
  }

  submitPost() {
    this.postToAdd = {
      title: this.formArray.get([0]).get('titleFormCtrl').value,
      lineNames: this.formArray.get([0]).get('lineFormCtrl').value,
      description: this.formArray.get([0]).get('descFormCtrl').value,
      latitude: this.formArray.get([1]).get('latFormCtrl').value,
      longitude: this.formArray.get([1]).get('lonFormCtrl').value,
      photos: this.formArray.get([2]).get('photoURLFormCtrl').value
    };

    this.postsService.addPost(this.postToAdd).subscribe({
      next: () => {
        this.showSpinner = false;
        this.isSubmitted = true;
        this.router.navigateByUrl('/events').then(() => this.snackbar.openSuccessSnackbar('Dodano post!'));
      },
      error: () => {
        this.showSpinner = false;
        this.snackbar.openErrorSnackbar('Wystąpił błąd podczas dodawania zdarzenia');
        // this.openInfoDialog('Wystąpił błąd podczas dodawania zdarzenia.', 'no');
      }
    });
  }

  updateLineInputValidity(valid: boolean) {
    this.lineInputIsValid = valid;
  }
}
