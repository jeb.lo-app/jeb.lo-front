import {async, ComponentFixture, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';

import {NewEventComponent} from './new-event.component';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {EventsComponent} from '../events/events.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {of} from 'rxjs';
import {Line} from '../../models/Line';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {LinesService} from '../../services/api/lines/lines.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialogModule} from '@angular/material/dialog';
import {MatStepperModule} from '@angular/material/stepper';
import {ChipAutocompleteInputComponent} from '../shared/chip-autocomplete-input/chip-autocomplete-input.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {PhotoUploaderComponent} from '../shared/photo-uploader/photo-uploader.component';

function getFormGroup(formBuilder) {
  return formBuilder.group({
    formArray: formBuilder.array([
      formBuilder.group({
        titleFormCtrl: ['', [Validators.required, Validators.maxLength(70)]],
        lineFormCtrl: [[], Validators.required],
        descFormCtrl: ['', [Validators.required, Validators.maxLength(256)]]
      }),
      formBuilder.group({
        latFormCtrl: ['', Validators.required],
        lonFormCtrl: ['', Validators.required]
      }),
      formBuilder.group({
        photoURLFormCtrl: [[]]
      })
    ])
  });
}

const mockLines: Line[] = [
  new Line({end1: 'Ccc', end2: 'Ddd', lineId: '2'}),
  new Line({end1: 'Aaa', end2: 'Bbb', lineId: '1'})
];
describe('NewEventComponent', () => {
  let component: NewEventComponent;
  let fixture: ComponentFixture<NewEventComponent>;
  const formBuilder: FormBuilder = new FormBuilder();
  const tokenStorage = jasmine.createSpyObj('TokenStorageService', {userIsLoggedIn: true, getUsername: 'test'});
  const linesService = jasmine.createSpyObj('LinesService', {getAllLines: of({body: mockLines})});
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientModule,
        MatDialogModule,
        MatStepperModule,
        MatSnackBarModule,
        MatAutocompleteModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(
          [{path: 'events', component: EventsComponent}]
        ),
      ],
      providers: [
        {provide: FormBuilder, useValue: formBuilder},
        {provide: TokenStorageService, useValue: tokenStorage},
        {provide: LinesService, useValue: linesService},
      ],
      declarations: [NewEventComponent, ChipAutocompleteInputComponent, PhotoUploaderComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEventComponent);
    component = fixture.componentInstance;
    component.formGroup = getFormGroup(formBuilder);
    component.lineInput = TestBed.createComponent(ChipAutocompleteInputComponent).componentInstance as ChipAutocompleteInputComponent;
    component.photoUploader = TestBed.createComponent(PhotoUploaderComponent).componentInstance as PhotoUploaderComponent;
    fixture.detectChanges();
  });

  it('should create', () => {
    spyOn(component.lineInput, 'initChosenAndAllValues');
    expect(component).toBeTruthy();
    expect(linesService.getAllLines).toHaveBeenCalled();
  });
});

describe('NewEventComponent fail', () => {
  let component: NewEventComponent;
  let fixture: ComponentFixture<NewEventComponent>;
  const formBuilder: FormBuilder = new FormBuilder();
  const tokenStorage = jasmine.createSpyObj('TokenStorageService', {userIsLoggedIn: false, getUsername: 'test'});
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientModule,
        MatDialogModule,
        MatStepperModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(
          [{path: 'events', component: EventsComponent}]
        ),
      ],
      providers: [
        {provide: FormBuilder, useValue: formBuilder},
        {provide: TokenStorageService, useValue: tokenStorage},
      ],
      declarations: [NewEventComponent, PhotoUploaderComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  it('should create and redirect', inject([Router], (router: Router) => {
    spyOn(router, 'navigateByUrl').and.returnValue(Promise.resolve(true));
    fixture = TestBed.createComponent(NewEventComponent);
    component = fixture.componentInstance;
    component.formGroup = getFormGroup(formBuilder);
    component.photoUploader = TestBed.createComponent(PhotoUploaderComponent).componentInstance as PhotoUploaderComponent;
    fixture.detectChanges();

    expect(component).toBeTruthy();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/events');
  }));
});
