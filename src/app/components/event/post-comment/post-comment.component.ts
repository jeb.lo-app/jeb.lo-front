import {Component, Input, OnInit} from '@angular/core';
import {PostComment} from '../../../models/post/PostComment';

@Component({
  selector: 'app-post-comment',
  templateUrl: './post-comment.component.html',
  styleUrls: ['./post-comment.component.scss']
})
export class PostCommentComponent implements OnInit {

  @Input()
  comment: PostComment;

  @Input()
  last: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
