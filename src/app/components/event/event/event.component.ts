import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {SnackbarService} from '../../../services/snackbar/snackbar.service';
import {Post} from '../../../models/post/Post';
import {PostsService} from '../../../services/api/posts/posts.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {DisplayMapComponent} from '../../shared/display-map/display-map.component';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {MatButton} from '@angular/material/button';
import {CommentsService} from '../../../services/api/comments/comments.service';
import {EventReportDialogComponent} from '../../shared/event-report-dialog/event-report-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {EventDeleteDialogComponent} from '../../shared/event-delete-dialog/event-delete-dialog.component';

@Component({
  selector: 'app-single-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit, AfterViewInit {

  @ViewChild(DisplayMapComponent)
  displayMap: DisplayMapComponent;

  @ViewChild('likeButton')
  likeButton: MatButton;

  @ViewChild('dislikeButton')
  dislikeButton: MatButton;

  @ViewChild('input')
  private commentInput: HTMLInputElement;

  post: Post;
  safePhotoURLs: SafeResourceUrl[];

  commentToAdd = '';

  constructor(private postsService: PostsService,
              private commentsService: CommentsService,
              private domSanitizer: DomSanitizer,
              private snackbar: SnackbarService,
              private dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private tokenStorageService: TokenStorageService,
              private router: Router) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.postsService.getEventWithId(parseInt(params.get('id'), 10)).subscribe({
        next: response => {
          this.post = response.body;
          this.safePhotoURLs = this.post.getSafePhotoURL(this.domSanitizer);
        },
        error: _ => {
          this.snackbar.openErrorSnackbar('Nie udało się załadować posta');
        }
      });
    });
  }

  ngAfterViewInit() {
    this.displayMap.updateMap();

    if (this.tokenStorageService.userIsLoggedIn() && this.post !== undefined) {
      this.likeButton.disabled = false;
      this.likeButton.disabled = false;
      if (this.post.userHasVoted === true) {
        this.likeButton.color = 'accent';
      } else if (this.post.userHasVoted === false) {
        this.dislikeButton.color = 'warn';
      }
    } else {
      this.likeButton.disabled = true;
      this.dislikeButton.disabled = true;
    }
  }

  openPostEdit() {
    this.router.navigateByUrl(`edit-event/${this.post.id}`);
  }

  openDeleteEventDialog() {
    const dialogRef = this.dialog.open(EventDeleteDialogComponent, {maxWidth: '20vw', minWidth: '350px'});

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.snackbar.openSuccessSnackbar(`delete? ${result}`);
    });
  }

  openReportEventDialog() {
    const dialogRef = this.dialog.open(EventReportDialogComponent, {maxWidth: '20vw', minWidth: '350px'});

    dialogRef.afterClosed().subscribe(async reportResult => {
      if (reportResult) {
        try {
          await this.postsService.reportPost(this.post.id, reportResult).toPromise();
          this.snackbar.openSuccessSnackbar('Przesłano zgłoszenie!');
        } catch (e) {
          this.snackbar.openErrorSnackbar('Wystąpił błąd podczas przesyłania zgłoszenia!');
        }
      }
    });
  }

  userIsLoggedIn() {
    return this.tokenStorageService.userIsLoggedIn();
  }

  userIsAuthor(): boolean {
    return this.post !== undefined && this.tokenStorageService.getUsername() === this.post.authorUsername;
  }

  clickLike() {
    if (this.post.userHasVoted === undefined) {
      this.postsService.voteForPost(this.post.id, {isLike: true}).subscribe({
        next: _ => {
          this.post.like();
          this.likeButton.color = 'accent';
          this.snackbar.openSuccessSnackbar('Przesłano głos!');
        },
        error: _ => {
          this.snackbar.openErrorSnackbar('Nie udało się przesłać głosu');
        }
      });
    } else if (this.post.userHasVoted === true) {
      this.postsService.voteForPost(this.post.id, {isLike: true}).subscribe({
        next: _ => {
          this.post.eraseVote();
          this.likeButton.color = undefined;
          this.snackbar.openSuccessSnackbar('Usunięto głos');
        },
        error: _ => {
          this.snackbar.openErrorSnackbar('Nie udało się usunąć głosu');
        }
      });
    } else if (this.post.userHasVoted === false) {
      this.postsService.voteForPost(this.post.id, {isLike: true}).subscribe({
        next: _ => {
          this.post.like();
          this.dislikeButton.color = undefined;
          this.likeButton.color = 'accent';
          this.snackbar.openSuccessSnackbar('Zmieniono głos!');
        },
        error: _ => {
          this.snackbar.openErrorSnackbar('Nie udało się zmienić głosu!');
        }
      });
    }
  }

  clickDislike() {
    if (this.post.userHasVoted === undefined) {
      this.postsService.voteForPost(this.post.id, {isLike: false}).subscribe({
        next: _ => {
          this.post.dislike();
          this.dislikeButton.color = 'warn';
          this.snackbar.openSuccessSnackbar('Przesłano głos!');
        },
        error: _ => {
          this.snackbar.openErrorSnackbar('Nie udało się przesłać głosu!');
        }
      });
    } else if (this.post.userHasVoted === false) {
      this.postsService.voteForPost(this.post.id, {isLike: false}).subscribe({
        next: _ => {
          this.post.eraseVote();
          this.dislikeButton.color = undefined;
          this.snackbar.openSuccessSnackbar('Usunięto głos!');
        },
        error: _ => {
          this.snackbar.openErrorSnackbar('Nie udało się usunąć głosu!');
        }
      });
    } else if (this.post.userHasVoted === true) {
      this.postsService.voteForPost(this.post.id, {isLike: false}).subscribe({
        next: _ => {
          this.post.dislike();
          this.likeButton.color = undefined;
          this.dislikeButton.color = 'warn';
          this.snackbar.openSuccessSnackbar('Zmieniono głos!');
        },
        error: _ => {
          this.snackbar.openErrorSnackbar('Nie udało się zmienić głosu!');
        }
      });
    }
  }

  addComment() {
    this.commentsService.addComment({text: this.commentToAdd, postId: this.post.id}).subscribe({
      next: _ => {
        this.snackbar.openSuccessSnackbar('Wysłano komentarz!');
        this.postsService.getEventWithId(this.post.id).subscribe({
          next: response => {
            this.post = response.body;
            this.safePhotoURLs = this.post.getSafePhotoURL(this.domSanitizer);
          },
          error: error => {
            console.error(error);
            this.snackbar.openErrorSnackbar('Nie udało się odświeżyć posta');
          }
        });
      },
      error: _ => {
        this.snackbar.openErrorSnackbar('Nie udało się wysłać komentarza!');
      }
    });
  }
}
