import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EventComponent} from './event/event.component';
import {MaterialModule} from '../../material/material.module';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import { PostCommentComponent } from './post-comment/post-comment.component';
import {FormsModule} from '@angular/forms';
import {MatCarouselModule} from '@ngmodule/material-carousel';

@NgModule({
  declarations: [
    EventComponent,
    PostCommentComponent
  ],
    imports: [
        CommonModule,
        MaterialModule,
        SharedModule,
        RouterModule,
        FormsModule,
        MatCarouselModule
    ],
  exports: [
    EventComponent
  ]
})
export class EventModule { }
