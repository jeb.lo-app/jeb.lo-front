import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bottom-nav',
  templateUrl: './bottom-navigation.component.html',
  styleUrls: ['./bottom-navigation.component.scss']
})
export class BottomNavigationComponent implements OnInit {

  @Input()
  isLoggedIn: boolean;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigate(section: string) {
    console.log('navigate to ' + section);

    switch (section) {
      case 'events': {
        this.router.navigateByUrl('/events');
        break;
      }
      case 'new-event': {
        this.router.navigateByUrl('/new-event');
        break;
      }
    }
  }

}
