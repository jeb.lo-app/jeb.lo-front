import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/api/auth/auth.service';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {SnackbarService} from '../../services/snackbar/snackbar.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  username: string;
  password: string;
  email: string;
  errorMessage: any;
  isRegisterFailed = false;

  form: FormGroup;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  loginFormControl = new FormControl('', [
    Validators.required,
  ]);

  constructor(private router: Router,
              private authService: AuthService,
              private tokenStorage: TokenStorageService,
              private snackbar: SnackbarService) { }

  reloadPage() {
    window.location.reload();
  }

  ngOnInit() {
    if (this.tokenStorage.userIsLoggedIn()) {
      this.router.navigateByUrl('/events').then(() => this.snackbar.openSuccessSnackbar('Zalogowano!'));
    }
    this.form = new FormGroup({
      email: this.emailFormControl,
      login: this.loginFormControl,
    });
  }

  register() {
    this.authService.register({username: this.username, email: this.email, password: this.password}).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveUser(data);
        this.isRegisterFailed = false;
        this.router.navigateByUrl('/events').then(() => this.snackbar.openSuccessSnackbar('Zarejestrowano!'));
        this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isRegisterFailed = true;
      }
    );

  }

}
