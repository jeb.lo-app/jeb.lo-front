import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {RegisterComponent} from './register.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {EventsComponent} from '../events/events.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthService} from '../../services/api/auth/auth.service';
import {of, throwError} from 'rxjs';
import {By} from '@angular/platform-browser';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(
          [{path: 'events', component: EventsComponent}]
        )
      ],
      declarations: [RegisterComponent]
    })
      .compileComponents();
  }));

  it('should create', inject([TokenStorageService], (tokenStorage: TokenStorageService) => {
    spyOn(tokenStorage, 'userIsLoggedIn').and.returnValue(false);
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  }));

  it('should create and redirect logged user',
    inject([Router, TokenStorageService],
      (router: Router, tokenStorageService: TokenStorageService) => {
        spyOn(router, 'navigateByUrl').and.returnValue(Promise.resolve(true));
        spyOn(tokenStorageService, 'userIsLoggedIn').and.returnValue(true);

        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        expect(component).toBeTruthy();
        expect(router.navigateByUrl).toHaveBeenCalledWith('/events');
      }));

  it('should register user', inject([Router, TokenStorageService, AuthService],
    (router: Router, tokenStorageService: TokenStorageService, authService: AuthService) => {
      const username = 'test';
      const password = 'loong-password';
      const email = 'some-email@email.com';
      const token = 'long-token';
      spyOn(router, 'navigateByUrl').and.returnValue(Promise.resolve(true));
      spyOn(tokenStorageService, 'saveToken').and.stub();
      spyOn(authService, 'register').and.returnValue(of({token}));

      fixture = TestBed.createComponent(RegisterComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      component.username = username;
      component.email = email;
      component.password = password;
      component.reloadPage = () => {};

      expect(component).toBeTruthy();
      fixture.debugElement.query(By.css('#register-btn')).triggerEventHandler('click', null);
      expect(authService.register).toHaveBeenCalledWith({username, email, password});
      expect(tokenStorageService.saveToken).toHaveBeenCalledWith(token);
      expect(router.navigateByUrl).toHaveBeenCalledWith('/events');
    }));

  it('should fail to register user', inject([AuthService],
    (authService: AuthService) => {
      const username = 'test';
      const password = 'loong-password';
      const email = 'some-email@email.com';
      spyOn(authService, 'register').and.returnValue(throwError({error: {message: 'Error'}}));

      fixture = TestBed.createComponent(RegisterComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      component.username = username;
      component.email = email;
      component.password = password;

      expect(component).toBeTruthy();
      fixture.debugElement.query(By.css('#register-btn')).triggerEventHandler('click', null);
      expect(authService.register).toHaveBeenCalledWith({username, email, password});
    }));
});
