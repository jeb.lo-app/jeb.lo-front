import {Component, OnInit, ViewChild} from '@angular/core';
import {Post, PostEditRequest} from '../../models/post/Post';
import {CoordinatesInputComponent} from '../shared/coordinates-input/coordinates-input.component';
import {DisplayMapComponent} from '../shared/display-map/display-map.component';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LinesService} from '../../services/api/lines/lines.service';
import {PostsService} from '../../services/api/posts/posts.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {SnackbarService} from '../../services/snackbar/snackbar.service';
import {ChipAutocompleteInputComponent} from '../shared/chip-autocomplete-input/chip-autocomplete-input.component';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {SimpleInfoDialogComponent} from '../shared/simple-info-dialog/simple-info-dialog.component';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {PhotoUploaderComponent} from '../shared/photo-uploader/photo-uploader.component';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {

  post: Post;
  postToSubmit: PostEditRequest;
  showSpinner = false;
  formGroup: FormGroup;
  allLines: string[];

  @ViewChild(CoordinatesInputComponent)
  private coordinatesInput: CoordinatesInputComponent;

  @ViewChild(DisplayMapComponent)
  private displayMap: DisplayMapComponent;

  @ViewChild(PhotoUploaderComponent)
  private photoUploader: PhotoUploaderComponent;

  @ViewChild(ChipAutocompleteInputComponent)
  lineInput: ChipAutocompleteInputComponent;

  get formArray(): AbstractControl | null {
    return this.formGroup.get('formArray');
  }
  constructor(private tokenStorage: TokenStorageService,
              private formBuilder: FormBuilder,
              private linesService: LinesService,
              private postsService: PostsService,
              private dialog: MatDialog,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private snackbar: SnackbarService) {
  }


  async ngOnInit() {
    this.showSpinner = true;

    if (!this.tokenStorage.userIsLoggedIn()) {
      return this.router.navigateByUrl('/events').then(() => this.snackbar.openErrorSnackbar('Nie jesteś zalogowany!'));
    }

    this.formGroup = this.formBuilder.group({
      formArray: this.formBuilder.array([
        this.formBuilder.group({
          titleFormCtrl: ['', [Validators.required, Validators.maxLength(70)]],
          lineFormCtrl: [[], Validators.required],
          descFormCtrl: ['', Validators.maxLength(256)]
        }),
        this.formBuilder.group({
          latFormCtrl: ['', Validators.required],
          lonFormCtrl: ['', Validators.required]
        }),
        this.formBuilder.group({
          photoURLFormCtrl: ['']
        })
      ])
    });

// Lines
    await this.linesService.getAllLines().subscribe(response => {
      this.allLines = [...new Set(response.body.map(line => line.lineName))];
      this.allLines.sort();
    });

    // postDownload
    this.activatedRoute.paramMap.subscribe(params => {
      this.postsService.getEventWithId(parseInt(params.get('id'), 10)).subscribe({
        next: response => {
          this.post = response.body;

          if (this.post.authorUsername !== this.tokenStorage.getUsername()) {
            return this.router.navigateByUrl('/events').then(() => this.snackbar.openErrorSnackbar('Nie masz dostępu do tej strony!'));
          }

          this.formArray.get([0]).get('titleFormCtrl').setValue(this.post.title);
          this.lineInput.initChosenAndAllValues(this.post.linesBlocked, this.allLines);
          this.formArray.get([0]).get('lineFormCtrl').setValue(this.post.linesBlocked.length > 0
            ? this.post.linesBlocked
            : null);
          this.formArray.get([0]).get('descFormCtrl').setValue(this.post.description);
          this.formArray.get([1]).get('latFormCtrl').setValue(this.post.coordinates.lat);
          this.formArray.get([1]).get('lonFormCtrl').setValue(this.post.coordinates.lng);
          this.formArray.get([2]).get('photoURLFormCtrl').setValue(this.post.photoSourceURLs);
          this.showSpinner = false;
        },
        error: () => {
          this.showSpinner = false;
          this.router.navigateByUrl('/events').then(() => this.openInfoDialog('Nie udało się załadować posta do edycji', 'yes'));
        }
      });
    });
  }

  onSelectionChanged(event: StepperSelectionEvent) {
    if (event.selectedIndex === 1) {
      this.coordinatesInput.initMap(this.post.coordinates);
    } else if (event.selectedIndex === 2) {
      this.photoUploader.initPhotoUploader(this.post.photoSourceURLs);
      this.displayMap.updateMap();
    }
  }

  updateCoordinates(coordinates: { lat: number, lng: number }) {
    this.formArray.get([1]).setValue({
      latFormCtrl: coordinates.lat,
      lonFormCtrl: coordinates.lng
    });
  }

  updatePhotosToSubmit(photosList: string[]) {
    this.formArray.get([2]).get('photoURLFormCtrl').setValue(photosList);
  }

  updateLines(updatedLines: string[]) {
    // HACK: in order to prevent the buttons from going to the next step, invisible form control is being updated
    this.formArray.get([0]).get('lineFormCtrl').setValue(updatedLines.length > 0 ? updatedLines : null);
  }

  submitPost() {
    this.postToSubmit = {
      title: this.formArray.get([0]).get('titleFormCtrl').value,
      lineNames: this.formArray.get([0]).get('lineFormCtrl').value,
      description: this.formArray.get([0]).get('descFormCtrl').value,
      latitude: this.formArray.get([1]).get('latFormCtrl').value,
      longitude: this.formArray.get([1]).get('lonFormCtrl').value,
      photos: this.formArray.get([2]).get('photoURLFormCtrl').value
    };

    this.postsService.editPost(this.postToSubmit, this.post.id).subscribe({
      next: () => {
        this.showSpinner = false;
        this.router.navigateByUrl('/events').then(() => this.snackbar.openSuccessSnackbar('Pomyślnie edytowano post!'));
      },
      error: () => {
        this.showSpinner = false;
        this.snackbar.openErrorSnackbar('Wystąpił błąd podczas edytowania zdarzenia');
      }
    });
  }

  openInfoDialog(dialogTitle: string, shouldReloadAfterClose: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;

    dialogConfig.data = {
      title: dialogTitle,
      reloadAfterClose: shouldReloadAfterClose
    };

    dialogConfig.panelClass = 'custom-dialog-background';

    const dialogRef = this.dialog.open(SimpleInfoDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(shouldReload => {
      if (shouldReload === 'yes') {
        this.router.navigate(['/']);
      }
    });
  }

}
