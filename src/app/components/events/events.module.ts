import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EventsComponent} from './events.component';
import {EventCardComponent} from './event-card/event-card.component';
import {MaterialModule} from '../../material/material.module';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import { EventFiltersComponent } from './event-filters/event-filters.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatChipsModule} from '@angular/material/chips';

@NgModule({
  declarations: [
    EventsComponent,
    EventCardComponent,
    EventFiltersComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MatDialogModule,
    MatSnackBarModule,
    SharedModule,
    RouterModule,
    MatButtonToggleModule,
    MatChipsModule
  ],
  exports: [
    EventsComponent,
    EventCardComponent
  ]
})
export class EventsModule { }
