import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { EventsComponent } from './events.component';
import {RouterTestingModule} from '@angular/router/testing';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';
import {PostsService} from '../../services/api/posts/posts.service';
import {of, throwError} from 'rxjs';
import {HttpClientModule, HttpResponse} from '@angular/common/http';
import {Post} from '../../models/post/Post';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import {NewEventComponent} from '../new-event/new-event.component';
import {LoginComponent} from '../login/login.component';
import {Filter} from '../../models/post/Filter';

describe('EventsComponent', () => {
  let component: EventsComponent;
  let fixture: ComponentFixture<EventsComponent>;
  const mockPosts: Post[] = [new Post({
    authorUsername: '',
    dislikes: 0,
    id: 0,
    latitude: 0,
    likes: 0,
    linesBlocked: [],
    longitude: 0,
    numberOfComments: 0,
    postedDate: '',
    statusPost: 'NOWE',
    title: '',
    urlPhotos: []
  }), new Post({
    authorUsername: '',
    dislikes: 0,
    id: 0,
    latitude: 0,
    likes: 0,
    linesBlocked: [],
    longitude: 0,
    numberOfComments: 0,
    postedDate: '',
    statusPost: 'AKTUALNE',
    title: '',
    urlPhotos: []
  })];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatSnackBarModule,
        RouterTestingModule.withRoutes(
          [
            {path: 'new-event', component: NewEventComponent},
            {path: 'login', component: LoginComponent}
          ]
        ),
        BrowserAnimationsModule,
      ],
      providers: [
      ],
      declarations: [ EventsComponent ]
    })
    .compileComponents();
  }));

  it('should create', inject([PostsService], (postsService: PostsService) => {
    spyOn(postsService, 'getAllEvents').and.returnValue(of(new HttpResponse({body: mockPosts})));
    fixture = TestBed.createComponent(EventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
    expect(postsService.getAllEvents).toHaveBeenCalled();
    expect(component.unfilteredPosts).toEqual(mockPosts);
    expect(component.posts).toEqual(mockPosts);
  }));

  it('should fail to load events', inject([PostsService], (postsService: PostsService) => {
    spyOn(postsService, 'getAllEvents').and.returnValue(throwError('test'));
    fixture = TestBed.createComponent(EventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(postsService.getAllEvents).toHaveBeenCalled();
  }));

  it('should navigate to add new event',
    inject([Router, TokenStorageService], (router: Router, tokenStorageService: TokenStorageService) => {
    spyOn(router, 'navigateByUrl').and.returnValue(Promise.resolve(true));
    spyOn(tokenStorageService, 'userIsLoggedIn').and.returnValue(true);

    fixture = TestBed.createComponent(EventsComponent);
    component = fixture.componentInstance;
    fixture.debugElement.query(By.css('button')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/new-event');
  }));

  it('should navigate to login',
    inject([Router, TokenStorageService], (router: Router, tokenStorageService: TokenStorageService) => {
      spyOn(router, 'navigateByUrl').and.returnValue(Promise.resolve(true));
      spyOn(tokenStorageService, 'userIsLoggedIn').and.returnValue(false);

      fixture = TestBed.createComponent(EventsComponent);
      component = fixture.componentInstance;
      fixture.debugElement.query(By.css('button')).triggerEventHandler('click', null);
      fixture.detectChanges();
      expect(router.navigateByUrl).toHaveBeenCalledWith('/login');
    }));

  it('should filter posts', inject([PostsService], (postsService: PostsService) => {
    spyOn(postsService, 'getAllEvents').and.returnValue(of(new HttpResponse({body: mockPosts})));
    fixture = TestBed.createComponent(EventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const filter: Filter[] = [
      new Filter('New', 'postStatus', 'NOWE', true),
      new Filter('New', 'postStatus', 'AKTUALNE', false),
      new Filter('Ended', 'postStatus', 'ZAKONCZONE', true)];

    component.filterPostsBy(filter);
    fixture.detectChanges();
    expect(component.unfilteredPosts).toEqual(mockPosts);
    mockPosts.pop();
    expect(component.posts).toEqual(mockPosts);
  }));
});
