import {Component, EventEmitter, Output} from '@angular/core';
import {Filter} from '../../../models/post/Filter';

@Component({
  selector: 'app-event-filters',
  templateUrl: './event-filters.component.html',
  styleUrls: ['./event-filters.component.css']
})
export class EventFiltersComponent {
  @Output()
  filterPosts = new EventEmitter();

  filters = [
    new Filter('Nowe zdarzenia', 'postStatus', 'NOWE', false),
    new Filter('Aktualne zdarzenia', 'postStatus', 'AKTUALNE', false),
    new Filter('Zakończone zdarzenia', 'postStatus', 'ZAKOŃCZONE', false),
  ];

  toggleFilter(filterIndex) {
    this.filters[filterIndex].toggleEnabled();
    this.filterPosts.emit(this.filters);
  }
}
