import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Post} from '../../../models/post/Post';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements OnInit {

  @Input()
  post: Post;

  photoURL: SafeResourceUrl;

  constructor(private domSanitizer: DomSanitizer,
              private router: Router) { }

  ngOnInit() {
    this.photoURL = this.post.getSafePhotoURL(this.domSanitizer)[0];
  }

  navigateToEvent() {
    this.router.navigateByUrl(`event/${this.post.id}`);
  }
}
