import {Component, OnInit} from '@angular/core';
import {Post} from '../../models/post/Post';
import {PostsService} from '../../services/api/posts/posts.service';
import {SnackbarService} from '../../services/snackbar/snackbar.service';
import {Filter} from '../../models/post/Filter';
import {TokenStorageService} from '../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  posts: Post[];
  unfilteredPosts: Post[];

  constructor(private postsService: PostsService,
              private snackbar: SnackbarService,
              private tokenStorageService: TokenStorageService,
              private router: Router) { }

  ngOnInit() {
    this.postsService.getAllEvents().subscribe({
      next: response => {
        this.unfilteredPosts = response.body;
        this.posts = this.unfilteredPosts;
      },
      error: _ => {
        this.snackbar.openErrorSnackbar('Nie udało się załadować postów');
        this.posts = [];
      }
    });
  }

  filterPostsBy(filters: Filter[]) {
    this.posts = this.unfilteredPosts.filter(post => this.filterBy(post, filters));
  }

  public filterBy(post: Post, filters: Filter[]) {
    let ret = null;
    for (const filter of filters) {
      if (filter.enabled) {
        ret = ret != null ? (post[filter.filterBy] === filter.filterValue || ret) : post[filter.filterBy] === filter.filterValue;
      }
    }
    return ret !== false;
  }

  navigateToAddEvent() {
    if (this.tokenStorageService.userIsLoggedIn()) {
      this.router.navigateByUrl(`/new-event`);
    } else {
      this.snackbar.openErrorSnackbar('Musisz się zalogować, aby móc dodać zdarzenie');
      this.router.navigateByUrl(`/login`);
    }
  }
}
