import {Component, OnInit, ViewChild} from '@angular/core';
import {AccountSettings} from '../../../models/user/AccountSettings';
import {ChipAutocompleteInputComponent} from '../../shared/chip-autocomplete-input/chip-autocomplete-input.component';
import {LinesService} from '../../../services/api/lines/lines.service';
import {StopsService} from '../../../services/api/stops/stops.service';
import {AccountService} from '../../../services/api/account/account.service';
import {TokenStorageService} from '../../../services/token-storage/token-storage.service';
import {Router} from '@angular/router';
import * as firebase from 'firebase';
import {SnackbarService} from '../../../services/snackbar/snackbar.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  notificationsAllowed: boolean = undefined;

  private notificationsSectionOpened = false;

  username: string;
  accountSettings: AccountSettings;

  @ViewChild('lineInput') lineInput: ChipAutocompleteInputComponent;
  @ViewChild('stopInput') stopInput: ChipAutocompleteInputComponent;

  constructor(private tokenStorageService: TokenStorageService,
              private linesService: LinesService,
              private stopsService: StopsService,
              private accountService: AccountService,
              private router: Router,
              private snackbar: SnackbarService) {
  }

  ngOnInit(): void {
    if (this.tokenStorageService.userIsLoggedIn()) {
      this.username = this.tokenStorageService.getUsername();
      this.accountService.getUserSettings().subscribe(response => {
        this.accountSettings = response.body;
      });
    } else {
      this.router.navigateByUrl('/events').then(_ => this.snackbar.openErrorSnackbar('Nie jesteś zalogowany!'));
    }
  }

  async loadValuesIntoLineAndStopInputs() {
    const linesResponsePromise = this.linesService.getAllLines().toPromise();
    const stopsResponsePromise = this.stopsService.getAllStops().toPromise();

    const linesResponse = await linesResponsePromise;
    const allLineNames = [...new Set(linesResponse.body.map(line => line.lineName))];
    this.lineInput.initChosenAndAllValues(this.accountSettings.subscriptionSettings.lineNames, allLineNames);

    const stopsResponse = await stopsResponsePromise;
    const allStopNames = [...new Set(stopsResponse.body.map(stop => stop.name))];
    this.stopInput.initChosenAndAllValues(this.accountSettings.subscriptionSettings.stopNames, allStopNames);
  }

  async openNotificationSettings() {
    if (this.notificationsSectionOpened === false) {
      this.notificationsSectionOpened = true;
      await this.loadValuesIntoLineAndStopInputs();

      const messaging = firebase.messaging();

      if (Notification.permission === 'granted') {
        this.notificationsAllowed = true;
      } else if (Notification.permission === 'denied') {
        this.notificationsAllowed = false;
      } else {
        const permission = await Notification.requestPermission();

        if (permission === 'granted') {

          const token = await messaging.getToken();

          try {
            const response = await this.accountService.sendFCMToken(token).toPromise();

            if (response.status === 200) {
              this.notificationsAllowed = true;
              // TODO: this is no longer necessary, should be deleted from both front-end and back-end model!
              this.accountSettings.subscribesToNotifications = true;
              this.updateUserSettings('Zapisano ustawienia powiadomień',
                `Nie udało się zapisać ustawień powiadomień. Zresetuj ustawienia powiadomień w przeglądarce.`);
            }
          } catch (error) {
            this.snackbar.openErrorSnackbar(
              'Wystąpił błąd podczas zapisywania ustawień powiadomień. Zresetuj ustawienia powiadomień w przeglądarce');
          }

        } else if (permission === 'denied') {
          this.notificationsAllowed = false;
          this.accountSettings.subscribesToNotifications = false;
          this.updateUserSettings('Zapisano ustawienia powiadomień',
            `Nie udało się zapisać ustawień powiadomień. Zresetuj ustawienia powiadomień w przeglądarce.`);
        }
      }
    }
  }

  updateLines(updatedLines: string[]) {
    this.accountSettings.subscriptionSettings.lineNames = updatedLines;
    this.updateUserSettings('Zapisano zmiany', 'Błąd przy zapisywaniu zmian');
  }

  updateStops(updatedStops: string[]) {
    this.accountSettings.subscriptionSettings.stopNames = updatedStops;
    this.updateUserSettings('Zapisano zmiany', 'Błąd przy zapisywaniu zmian');
  }

  private updateUserSettings(successMessage: string, errorMessage: string) {
    this.accountService.updateUserSettings(this.accountSettings.toUpdateRequest()).subscribe({
      next: _ => {
        if (successMessage) {
          this.snackbar.openSuccessSnackbar(successMessage);
        }
      },
      error: _ => {
        if (errorMessage) {
          this.snackbar.openErrorSnackbar(errorMessage);
        }
      }
    });
  }
}
