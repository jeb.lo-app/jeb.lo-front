import { Injectable } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const SENT_TOKEN_TO_SERVER = 'sent-token-to-server';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor(private cookieService: CookieService) { }

  signOut() {
    this.cookieService.delete('token', TOKEN_KEY);
    window.localStorage.clear();
  }

  public saveToken(token: string) {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user) {
    window.localStorage.removeItem(USER_KEY);
    window.localStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser() {
    return JSON.parse(localStorage.getItem(USER_KEY));
  }

  public getUsername() {
    return this.userIsLoggedIn() ? JSON.parse(localStorage.getItem(USER_KEY)).username : undefined;
  }

  public userIsLoggedIn() {
    return localStorage.getItem(TOKEN_KEY) !== null;
  }

  public setFCMTokenSentToServer(sent: boolean) {
    window.localStorage.setItem(SENT_TOKEN_TO_SERVER, sent ? '1' : '0');
  }

  public isFCMTokenSentToServer(): boolean {
    return window.localStorage.getItem(SENT_TOKEN_TO_SERVER) === '1';
  }

}
