import { TestBed } from '@angular/core/testing';

import { TokenStorageService } from './token-storage.service';
import {User} from '../../models/user/User';
import {CookieService} from 'ngx-cookie-service';

describe('TokenStorageService', () => {
  let service: TokenStorageService;
  let cookieServiceMock: jasmine.SpyObj<CookieService>;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TokenStorageService);
    cookieServiceMock = jasmine.createSpyObj<CookieService>('CookieService', ['check', 'set', 'delete']);
    cookieServiceMock.check.and.returnValue(true);

    let store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };
    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem')
      .and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear')
      .and.callFake(mockLocalStorage.clear);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should save token', () => {
    const token = 'pesssuntclabularesdeemeritiscedrium';
    service.saveToken(token);
    expect(localStorage.getItem('auth-token')).toEqual(token);
  });

  it('should get token', () => {
    const token = 'pesssuntclabularesdeemeritiscedrium';
    localStorage.setItem('auth-token', token);
    expect(service.getToken()).toEqual(token);
  });

  it('should save user', () => {
    const user: User = {
      get joinedDateAsString(): string {
        return '2020-05-01';
      },
      joinedDate: new Date('2020-05-01'),
      numberOfComments: 0,
      numberOfPosts: 0,
      numberOfUpvotes: 0,
      photoSourceURL: '',
      username: ''
    };
    service.saveUser(user);
    expect(localStorage.getItem('auth-user')).toEqual(JSON.stringify(user));
  });

  it('should get user', () => {
    const user = {
      get joinedDateAsString(): string {
        return '2020-05-01';
      },
      joinedDate: '2024-05-01T00:00:00.000Z',
      numberOfComments: 0,
      numberOfPosts: 0,
      numberOfUpvotes: 0,
      photoSourceURL: '',
      username: ''
    };
    localStorage.setItem('auth-user', JSON.stringify(user));
    expect(service.getUser()).toEqual(user);
  });
  it('should get username', () => {
    const username = 'testTest';
    localStorage.setItem('auth-token', 'token');
    localStorage.setItem('auth-user', JSON.stringify({username}));
    expect(service.getUsername()).toBe(username);
  });

  it('should fail to get username', () => {
    expect(service.getUsername()).toBe(undefined);
  });

  it('should set FCM token sent', () => {
    service.setFCMTokenSentToServer(true);
    expect(localStorage.getItem('sent-token-to-server')).toBe('1');

    service.setFCMTokenSentToServer(false);
    expect(localStorage.getItem('sent-token-to-server')).toBe('0');
  });

  it('should get FCM token sent', () => {
    localStorage.setItem('sent-token-to-server', '1');
    expect(service.isFCMTokenSentToServer()).toBeTruthy();

    localStorage.setItem('sent-token-to-server', '0');
    expect(service.isFCMTokenSentToServer()).toBeFalsy();
  });

  it('should signout', () => {
    localStorage.setItem('auth-token', 'token');
    cookieServiceMock.set('token', 'token');
    service.signOut();
    expect(localStorage.getItem('auth-token')).toBeFalsy();
  });
});
