import { Injectable } from '@angular/core';
import {Post, PostAddRequest, PostEditRequest, PostResponse} from '../../../models/post/Post';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';
import {PostVote} from '../../../models/post/PostVote';
import {ReportRequest} from '../../../models/post/ReportRequest';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  public readonly apiURL = `${environment.apiURL}/posts`;

  constructor(private http: HttpClient) {
  }

  public getEventWithId(id: number): Observable<HttpResponse<Post>> {
    return this.http.get<PostResponse>(`${this.apiURL}/${id}`, {observe: 'response'}).pipe(
      map((response: HttpResponse<PostResponse>) => {
        return new HttpResponse<Post>({
          body: new Post(response.body),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public getAllEvents(lines = null, status = null, from = null, to = null): Observable<HttpResponse<Post[]>> {
    let params = new HttpParams();
    if (lines != null) { params = params.set('lines', lines); }
    if (status != null) { params = params.set('status', status); }
    if (from != null) { params = params.set('from', from); }
    if (to != null) { params = params.set('to', to); }
    return this.http.get<PostResponse[]>(`${this.apiURL}/`, {observe: 'response', params}).pipe(
      map((response: HttpResponse<PostResponse[]>) => {
        return new HttpResponse<Post[]>({
          body: response.body.map(postResponse => new Post(postResponse)),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public addPost(postToAdd: PostAddRequest) {
    return this.http.post<PostAddRequest>(`${this.apiURL}/`, postToAdd, {observe: 'response'});
  }

  public editPost(postToEdit: PostEditRequest, id: number) {
    return this.http.put<PostEditRequest>(`${this.apiURL}/${id}`, postToEdit, {observe: 'response'});
  }

  public voteForPost(id: number, postVote: PostVote): Observable<HttpResponse<any>> {
    return this.http.post(`${this.apiURL}/${id}/vote`, postVote, {observe: 'response'});
  }

  public reportPost(id: number, report: ReportRequest): Observable<HttpResponse<any>> {
    return this.http.post(`${this.apiURL}/${id}/report`, report, {observe: 'response'});
  }
}
