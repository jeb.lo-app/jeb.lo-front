import {TestBed} from '@angular/core/testing';

import {PostsService} from './posts.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Post, PostAddRequest, PostEditRequest} from '../../../models/post/Post';
import {PostVote} from '../../../models/post/PostVote';
import {ReportRequest} from '../../../models/post/ReportRequest';

describe('PostService', () => {
  let service: PostsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PostsService]
    });
    service = TestBed.inject(PostsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all posts', () => {
    const responseBody = [
      {
        id: 102,
        title: 'Test1',
        postedDate: '2020-02-03T15:06:56',
        statusPost: 'AKTUALNE',
        authorUsername: 'test1',
        linesBlocked: ['15'],
        likes: 2,
        dislikes: 1,
        urlPhotos: [
          'test.jpg',
          'test2.jpg'
        ],
        numberOfComments: 1,
        latitude: 51.16418,
        longitude: 17.111937
      }, {
        id: 104,
        title: 'Test2',
        postedDate: '2020-02-03T15:06:56',
        statusPost: 'AKTUALNE',
        authorUsername: 'test2',
        linesBlocked: ['4', '5', '11'],
        likes: 1,
        dislikes: 4,
        urlPhotos: ['test.jpg'],
        numberOfComments: 2,
        latitude: 51.097866,
        longitude: 16.993462
      }
    ];

    const checkResponse = (response) => {
      expect(response.status).toBe(200);
      expect(response.statusText).toBe('OK');
      expect(response.body.length).toBe(2);
      expect(response.body.pop()).toBeTruthy();
      expect(response.body.pop()).toEqual(new Post(responseBody[0]));
    };

    const sendRequest = (path) => {
      const request = httpMock.expectOne(`${service.apiURL}/${path}`);
      expect(request.request.method).toBe('GET');
      request.flush(responseBody);
      httpMock.verify();
    };

    service.getAllEvents().subscribe(response => checkResponse(response));
    sendRequest('');

    service.getAllEvents([10, 15]).subscribe(response => checkResponse(response));
    sendRequest('?lines=10,15');

    service.getAllEvents(null, 'nowe').subscribe(response => checkResponse(response));
    sendRequest('?status=nowe');

    service.getAllEvents(null,  null, '2021-04-20').subscribe(response => checkResponse(response));
    sendRequest('?from=2021-04-20');

    service.getAllEvents(null,  null, null, '2021-05-01').subscribe(response => checkResponse(response));
    sendRequest('?to=2021-05-01');
  });

  it('should get post with id', () => {
    const responseBody = {
      id: 100,
      title: 'Test1',
      postedDate: '2020-02-03T15:06:56',
      statusPost: 'AKTUALNE',
      authorUsername: 'test1',
      linesBlocked: ['15'],
      likes: 2,
      dislikes: 1,
      urlPhotos: [
        'test.jpg',
        'test2.jpg'
      ],
      numberOfComments: 1,
      latitude: 51.16418,
      longitude: 17.111937
    };

    service.getEventWithId(responseBody.id).subscribe(response => {
      expect(response.status).toBe(200);
      expect(response.statusText).toBe('OK');
      expect(response.body).toBeTruthy();
      expect(response.body).toEqual(new Post(responseBody));
    });

    const request = httpMock.expectOne(`${service.apiURL}/${responseBody.id}`);
    expect(request.request.method).toBe('GET');
    request.flush(responseBody);
    httpMock.verify();
  });

  it('should fail to get post with id', () => {
    const id = 101;
    service.getEventWithId(id).subscribe(
      _ => fail('should have failed with 404'),
      error => expect(error.status).toBe(404));

    const request = httpMock.expectOne(`${service.apiURL}/${id}`);
    expect(request.request.method).toBe('GET');
    request.flush({}, {status: 404, statusText: 'Not Found'});
    httpMock.verify();
  });

  it('should add new post', () => {
    const requestBody: PostAddRequest = {
      description: 'Test test',
      latitude: 51,
      lineNames: ['A'],
      longitude: 10,
      photos: [''],
      title: 'Test1'
    };

    service.addPost(requestBody).subscribe(response => {
      expect(response.status).toBe(201);
      expect(response.statusText).toBe('CREATED');
      expect(response.body).toBeTruthy();
    });

    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('POST');
    request.flush({}, {
      status: 201,
      statusText: 'CREATED'
    });
    httpMock.verify();
  });

  it('should fail add new post', () => {
    const requestBody: PostAddRequest = {
      description: 'Test test',
      latitude: 51,
      lineNames: ['A'],
      longitude: 10,
      photos: [''],
      title: 'Test1'
    };

    service.addPost(requestBody).subscribe(
      _ => fail('should have failed with 400'),
      error => expect(error.status).toBe(400));

    let request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 400, statusText: 'Bad Request'});
    httpMock.verify();

    service.addPost(requestBody).subscribe(
      _ => fail('should have failed with 404'),
      error => expect(error.status).toBe(404));

    request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 404, statusText: 'Not Found'});
    httpMock.verify();
  });

  it('should edit post', () => {
    const requestBody: { id: number, post: PostEditRequest } = {
      id: 1,
      post: {
        description: 'Test test',
        latitude: 51,
        lineNames: ['A'],
        longitude: 10,
        photos: [''],
        title: 'Test1'
      }
    };

    service.editPost(requestBody.post, requestBody.id).subscribe(response => {
      expect(response.status).toBe(200);
      expect(response.statusText).toBe('OK');
    });

    const request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}`);
    expect(request.request.method).toBe('PUT');
    request.flush({});
    httpMock.verify();
  });

  it('should fail to edit post', () => {
    const requestBody: { id: number, post: PostEditRequest } = {
      id: 1,
      post: {
        description: 'Test test',
        latitude: 51,
        lineNames: ['A'],
        longitude: 10,
        photos: [''],
        title: 'Test1'
      }
    };

    service.editPost(requestBody.post, requestBody.id).subscribe(
      _ => fail('should have failed with 400'),
      error => expect(error.status).toBe(400));

    let request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}`);
    expect(request.request.method).toBe('PUT');
    request.flush({}, {status: 400, statusText: 'Bad request'});
    httpMock.verify();

    service.editPost(requestBody.post, requestBody.id).subscribe(
      _ => fail('should have failed with 403'),
      error => expect(error.status).toBe(403));

    request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}`);
    expect(request.request.method).toBe('PUT');
    request.flush({}, {status: 403, statusText: 'Forbidden'});
    httpMock.verify();

    service.editPost(requestBody.post, requestBody.id).subscribe(
      _ => fail('should have failed with 404'),
      error => expect(error.status).toBe(404));

    request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}`);
    expect(request.request.method).toBe('PUT');
    request.flush({}, {status: 404, statusText: 'Not found'});
    httpMock.verify();
  });

  it('should edit vote to post', () => {
    const requestBody: { id: number, postVote: PostVote } = {
      id: 1,
      postVote: {
        isLike: true,
      }
    };
    const responseBody = {
      numberOfLikes: 2,
      numberOFDislikes: 2
    };

    service.voteForPost(requestBody.id, requestBody.postVote).subscribe(response => {
      expect(response.status).toBe(200);
      expect(response.statusText).toBe('OK');
      expect(response.body).toBeTruthy();
      expect(response.body).toEqual(responseBody);
    });

    const request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}/vote`);
    expect(request.request.method).toBe('POST');
    request.flush(responseBody);
    httpMock.verify();
  });

  it('should fail to edit vote to post', () => {
    const requestBody: { id: number, postVote: PostVote } = {
      id: 1,
      postVote: {
        isLike: true,
      }
    };

    service.voteForPost(requestBody.id, requestBody.postVote).subscribe(
      _ => fail('should have failed with 400'),
      error => expect(error.status).toBe(400));

    const request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}/vote`);
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 400, statusText: 'Bad Request'});
    httpMock.verify();
  });

  it('should report post', () => {
    const requestBody: {id: number, report: ReportRequest} = {
      id: 1,
      report: {
        reason: 'test reason',
        content: 'test test test'
      }
    };

    service.reportPost(requestBody.id, requestBody.report).subscribe(response => {
      expect(response.status).toBe(201);
      expect(response.statusText).toBe('CREATED');
    });

    const request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}/report`);
    expect(request.request.method).toBe('POST');
    request.flush({}, {
      status: 201,
      statusText: 'CREATED'
    });
    httpMock.verify();
  });

  it('should fail to report post', () => {
    const requestBody: {id: number, report: ReportRequest} = {
      id: 1,
      report: {
        reason: 'test reason',
        content: 'test test test'
      }
    };

    service.reportPost(requestBody.id, requestBody.report).subscribe(
      _ => fail('should have failed with 400'),
      error => expect(error.status).toBe(400));

    let request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}/report`);
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 400, statusText: 'Bad Request'});
    httpMock.verify();

    service.reportPost(requestBody.id, requestBody.report).subscribe(
      _ => fail('should have failed with 404'),
      error => expect(error.status).toBe(404));

    request = httpMock.expectOne(`${service.apiURL}/${requestBody.id}/report`);
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 404, statusText: 'Not found'});
    httpMock.verify();
  });
});
