import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Stop, StopResponse} from '../../../models/Stop';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StopsService {

  public readonly apiURL = `${environment.apiURL}/stops`;

  constructor(private http: HttpClient) { }

  public getAllStops(): Observable<HttpResponse<Stop[]>> {
    return this.http.get<StopResponse[]>(`${this.apiURL}/`, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<Stop[]>({
          body: response.body.map(stopResponse => new Stop(stopResponse)),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }
}
