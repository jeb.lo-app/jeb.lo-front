import {TestBed} from '@angular/core/testing';

import {StopsService} from './stops.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Stop} from '../../../models/Stop';

describe('StopsService', () => {
  let service: StopsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [StopsService]
    });
    service = TestBed.inject(StopsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all stops', () => {
    const responseBody = [
      {stopId: 79, stopCode: 18371, stopName: 'Wolska', stopLat: 51.14666, stopLon: 16.869354},
      {stopId: 4424, stopCode: 31911002, stopName: 'Kępa (na wys. nr 36)', stopLat: 51.24044, stopLon: 17.20007},
      {stopId: 679, stopCode: 23510, stopName: 'LIPA PIOTROWSKA', stopLat: 51.17382, stopLon: 16.994226},
      {stopId: 3979, stopCode: 92056016, stopName: 'Miękinia Cegielnia', stopLat: 51.188244, stopLon: 16.724655}
    ];

    service.getAllStops().subscribe(response => {
      expect(response.status).toBe(200);
      expect(response.statusText).toBe('OK');
      expect(response.body.length).toBe(4);
      expect(response.body.pop()).toBeTruthy();
      expect(response.body.pop()).toEqual(new Stop(responseBody[2]));
    });


    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('GET');
    request.flush(responseBody);
    httpMock.verify();
  });
});
