import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {User, UserResponse} from '../../../models/user/User';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClient, HttpResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public readonly apiURL = `${environment.apiURL}/accounts`;

  constructor(private http: HttpClient) { }

  public getUserDataByUsername(username: string): Observable<HttpResponse<User>> {
    return this.http.get<UserResponse>(`${this.apiURL}/${username}`, {observe: 'response'}).pipe(
      map((response: HttpResponse<UserResponse>) => {
        return new HttpResponse<User>({
          body: new User(response.body),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }
}
