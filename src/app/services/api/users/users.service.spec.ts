import {TestBed} from '@angular/core/testing';

import {UsersService} from './users.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('UsersService', () => {
  let service: UsersService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UsersService]
    });
    service = TestBed.inject(UsersService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  it('should user data by username', () => {
    const responseBody = {
      username: 'presiden69',
      joinedDate: '2020-01-23T11:31:06',
      photoSource: 'https://randomuser.me/api/portraits/men/24.jpg',
      numberOfPosts: 1,
      numberOfComments: 1,
      likes: 4,
      dislikes: 2,
      favoriteLines: [],
      favoriteStops: [],
      accountPosts: [{
        id: 102,
        title: 'Azaliowa',
        description: 'Test!!!!!1!11!',
        postedDate: '2020-02-03T15:06:56',
        statusPost: 'AKTUALNE',
        linesBlocked: ['15'],
        likes: 2,
        dislikes: 1,
        numberOfComments: 1
      }]
    };

    service.getUserDataByUsername(responseBody.username).subscribe(response => {
      expect(response.status).toBe(200);
      expect(response.statusText).toBe('OK');
      expect(response.body.username).toBe(responseBody.username);
      expect(response.body.joinedDate).toEqual(new Date(responseBody.joinedDate));
      expect(response.body.numberOfComments).toBe(responseBody.numberOfComments);
      expect(response.body.numberOfPosts).toBe(responseBody.numberOfPosts);
      expect(response.body.numberOfUpvotes).toBe(responseBody.likes);
      expect(response.body.photoSourceURL).toBe(responseBody.photoSource);
    });

    const request = httpMock.expectOne(`${service.apiURL}/${responseBody.username}`);
    expect(request.request.method).toBe('GET');

    request.flush(responseBody);
    httpMock.verify();
  });

  it('should fail to get user data by username', () => {
    const username = 'test';
    service.getUserDataByUsername(username).subscribe(
      _ => fail('should have failed with 404'),
      error => expect(error.status).toBe(404));

    const request = httpMock.expectOne(`${service.apiURL}/${username}`);
    expect(request.request.method).toBe('GET');

    request.flush({}, {status: 404, statusText: 'Not Found'});
    httpMock.verify();
  });
});
