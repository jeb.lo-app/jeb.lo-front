import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Line, LineResponse} from '../../../models/Line';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LinesService {

  public readonly apiURL = `${environment.apiURL}/lines`;

  constructor(private http: HttpClient) { }

  public getAllLines(): Observable<HttpResponse<Line[]>> {
    return this.http.get<LineResponse[]>(`${this.apiURL}/`, {observe: 'response'}).pipe(
      map((response: HttpResponse<LineResponse[]>) => {
        return new HttpResponse<Line[]>({
          body: response.body.map(lineResponse => new Line(lineResponse)),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }
}
