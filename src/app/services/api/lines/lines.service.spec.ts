import { TestBed } from '@angular/core/testing';

import { LinesService } from './lines.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('LinesService', () => {
  let service: LinesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LinesService]
    });
    service = TestBed.inject(LinesService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all lines', () => {
    service.getAllLines().subscribe(response => {
      expect(response.status).toBe(200);
      expect(response.statusText).toBe('OK');
      expect(response.body.length).toBe(2);
      expect(response.body.pop()).toBeTruthy();
    });

    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('GET');
    request.flush([{
      lineId: '1',
      end1: 'Kromera',
      end2: 'Pilczyce'
    }, {
      lineId: 'A',
      end1: 'Pl. Grunwaldzki',
      end2: 'Gielda'
    }]);
    httpMock.verify();
  });
});
