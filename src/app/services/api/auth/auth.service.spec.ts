import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService]
    });
    service = TestBed.inject(AuthService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should login user', () => {
    const loginRequest = {
      username: 'test',
      password: 'testowe',
    };
    const token = 'aaaa';
    const role = 'admin';

    service.login(loginRequest).subscribe(response => {
      expect(response.token).toBe(token);
      expect(response.role).toBe(role);
    });

    const request = httpMock.expectOne(service.apiURL + '/signin');
    expect(request.request.method).toBe('POST');
    request.flush({
      token,
      role,
    });
    httpMock.verify();
  });

  it('should fail to login user', () => {
    const loginRequest = {
      username: 'test',
      password: 'testowe',
    };

    service.login(loginRequest).subscribe(
      _ => fail('should have failed with 404'),
      error => expect(error.status).toBe(401));

    const request = httpMock.expectOne(service.apiURL + '/signin');
    expect(request.request.method).toBe('POST');
    request.flush({}, {
      status: 401,
      statusText: 'Unauthorized'
    });
    httpMock.verify();
  });

  it('should register user', () => {
    const registerRequest = {
      username: 'test',
      password: 'testowe',
      email: 'test@test.com',
    };
    const token = 'aaaa';
    const role = 'admin';

    service.register(registerRequest).subscribe(response => {
      expect(response.token).toBe(token);
      expect(response.role).toBe(role);
    });

    const request = httpMock.expectOne(service.apiURL + '/signup');
    expect(request.request.method).toBe('POST');
    request.flush({
      token,
      role,
    });
    httpMock.verify();
  });

  it('should fail to register user', () => {
    const registerRequest = {
      username: 'test',
      password: 'testowe',
      email: 'test@test.com',
    };

    service.register(registerRequest).subscribe(
      _ => fail('should have failed with 400'),
      error => expect(error.status).toBe(400));

    let request = httpMock.expectOne(service.apiURL + '/signup');
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 400, statusText: 'Bad request'});
    httpMock.verify();

    service.register(registerRequest).subscribe(
      _ => fail('should have failed with 500'),
      error => expect(error.status).toBe(500));

    request = httpMock.expectOne(service.apiURL + '/signup');
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 500, statusText: 'Internal server error'});
    httpMock.verify();
  });
});
