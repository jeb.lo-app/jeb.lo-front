import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';
import { tap } from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'}),
};

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  public readonly apiURL = `${environment.apiURL}/auth`;

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  login(credentials): Observable<any> {
    return this.http.post(this.apiURL + '/signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions)
      .pipe(
      tap((usrLogin) => {
        this.cookieService.set('token', usrLogin.token);
        localStorage.setItem('role', usrLogin.role);
      })
    );
  }

  register(credentials): Observable<any> {
    return this.http.post(this.apiURL + '/signup', {
      username: credentials.username,
      email: credentials.email,
      password: credentials.password
    }, httpOptions);
  }
}
