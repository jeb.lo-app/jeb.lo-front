import {TestBed} from '@angular/core/testing';

import {AccountService} from './account.service';
import {AccountSettingsUpdateRequest} from '../../../models/user/AccountSettings';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('AccountService', () => {
  let service: AccountService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AccountService]
    });
    service = TestBed.inject(AccountService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get userSettings', () => {
    service.getUserSettings().subscribe(accountSettings => {
      expect(accountSettings.status).toBe(200);
      expect(accountSettings.body.subscribesToNotifications).toBeFalsy();
      expect(accountSettings.body.avatarPhotoURL).toBe('');
      expect(accountSettings.body.subscriptionSettings.lineNames.length).toBe(0);
      expect(accountSettings.body.subscriptionSettings.stopNames.length).toBe(0);
    });

    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('GET');
    request.flush({
      photo: '',
      isSubscribe: false,
      subscriptionSettings: {
        lineNames: [],
        stopNames: [],
      },
    });
    httpMock.verify();
  });

  it('should fail to get userSettings', () => {
    service.getUserSettings().subscribe(
      _ => fail('should have failed with 404'),
        error => expect(error.status).toBe(404)
    );

    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('GET');
    request.flush({}, {status: 404, statusText: 'Not Found'});
    httpMock.verify();
  });

  it('should update userSettings', () => {
    const updateRequest: AccountSettingsUpdateRequest = {
      isSubscribe: true,
      photo: 'test/test.png',
      subscriptionSettings: undefined
    };

    service.updateUserSettings(updateRequest).subscribe(accountSettings => {
      expect(accountSettings.status).toBe(200);
      expect(accountSettings.body.isSubscribe).toBeTruthy();
      expect(accountSettings.body.photo).toBe(updateRequest.photo);
      expect(accountSettings.body.subscriptionSettings.lineNames.length).toBe(0);
      expect(accountSettings.body.subscriptionSettings.stopNames.length).toBe(0);
    });

    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('PUT');
    request.flush({
      photo: 'test/test.png',
      isSubscribe: true,
      subscriptionSettings: {
        lineNames: [],
        stopNames: [],
      },
    });
    httpMock.verify();
  });

  it('should fail to update userSettings', () => {
    const updateRequest: AccountSettingsUpdateRequest = {
      isSubscribe: true,
      photo: 'test/test.png',
      subscriptionSettings: undefined
    };

    service.updateUserSettings(updateRequest).subscribe(
      _ => fail('should have failed with 404'),
      error => expect(error.status).toBe(404)
    );

    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('PUT');
    request.flush({}, {status: 404, statusText: 'Not Found'});
    httpMock.verify();
  });

  it('should send FCM token', () => {
    const token = 'aaaa';

    service.sendFCMToken(token).subscribe(fcmResponse => {
      expect(fcmResponse.status).toBe(201);
      expect(fcmResponse.statusText).toBe('CREATED');
    });

    const request = httpMock.expectOne(`${service.apiURL}/token/fcm/${token}`);
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 201, statusText: 'CREATED'});
    httpMock.verify();
  });

  it('should fail to send FCM token', () => {
    const token = 'aaaa';

    service.sendFCMToken(token).subscribe(_ => fail('should have failed with 400'),
      error => expect(error.status).toBe(400));

    const request = httpMock.expectOne(`${service.apiURL}/token/fcm/${token}`);
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 400, statusText: 'Bad Request'});
    httpMock.verify();
  });
});
