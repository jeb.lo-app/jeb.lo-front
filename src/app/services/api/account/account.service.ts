import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {
  AccountSettings,
  AccountSettingsResponse,
  AccountSettingsUpdateRequest
} from '../../../models/user/AccountSettings';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  public readonly apiURL = `${environment.apiURL}/accounts`;

  constructor(private http: HttpClient) { }

  public getUserSettings(): Observable<HttpResponse<AccountSettings>> {
    return this.http.get<AccountSettingsResponse>(`${this.apiURL}/`, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<AccountSettings>({
          body: new AccountSettings(response.body),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public updateUserSettings(accountSettings: AccountSettingsUpdateRequest) {
    return this.http.put<AccountSettingsUpdateRequest>(`${this.apiURL}/`, accountSettings, {observe: 'response'});
  }

  public sendFCMToken(token: string): Observable<HttpResponse<any>> {
    return this.http.post(`${this.apiURL}/token/fcm/${token}`, {}, {observe: 'response'});
  }
}
