import { TestBed } from '@angular/core/testing';

import { CommentsService } from './comments.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {PostCommentAddRequest} from '../../../models/post/PostComment';

describe('CommentsService', () => {
  let service: CommentsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CommentsService]
    });
    service = TestBed.inject(CommentsService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add comment', () => {
    const commentRequest: PostCommentAddRequest = {
      postId: 0,
      text: 'test'
    };

    service.addComment(commentRequest).subscribe(response => {
      expect(response.status).toBe(201);
      expect(response.statusText).toBe('CREATED');
    });

    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 201, statusText: 'CREATED'});
    httpMock.verify();
  });

  it('should fail to add comment', () => {
    const commentRequest: PostCommentAddRequest = {
      postId: 0,
      text: 'test'
    };

    service.addComment(commentRequest).subscribe(
      _ => fail('should have failed with 400'),
      error => expect(error.status).toBe(400));

    const request = httpMock.expectOne(service.apiURL + '/');
    expect(request.request.method).toBe('POST');
    request.flush({}, {status: 400, statusText: 'Bad Request'});
    httpMock.verify();
  });
});
