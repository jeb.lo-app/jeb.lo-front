import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {PostCommentAddRequest} from '../../../models/post/PostComment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  public readonly apiURL = `${environment.apiURL}/comments`;

  constructor(private http: HttpClient) { }

  public addComment(commentToAdd: PostCommentAddRequest) {
    return this.http.post<PostCommentAddRequest>(`${this.apiURL}/`, commentToAdd, {observe: 'response'});
  }
}
