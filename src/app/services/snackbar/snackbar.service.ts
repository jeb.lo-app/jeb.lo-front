import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {IconSnackbarComponent} from '../../components/shared/icon-snackbar/icon-snackbar.component';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  private defaultSnackbarDuration = 2000;

  constructor(private snackBar: MatSnackBar) { }

  openSuccessSnackbar(message: string) {
    this.snackBar.openFromComponent(IconSnackbarComponent, {
      data: {
        icon: 'done', message
      },
      duration: this.defaultSnackbarDuration, panelClass: 'snackbar-success'
    });
  }

  openErrorSnackbar(message: string) {
    this.snackBar.openFromComponent(IconSnackbarComponent, {
      data: {icon: 'error', message},
      duration: this.defaultSnackbarDuration,
      panelClass: 'snackbar-error'
    });
  }
}
