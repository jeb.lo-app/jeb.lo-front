import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EventComponent} from '../components/event/event/event.component';
import {EventsComponent} from '../components/events/events.component';
import {NewEventComponent} from '../components/new-event/new-event.component';
import {ProfileComponent} from '../components/profile/profile/profile.component';
import {LoginComponent} from '../components/login/login.component';
import {RegisterComponent} from '../components/register/register.component';
import {SettingsComponent} from '../components/settings/settings/settings.component';
import {EditEventComponent} from '../components/edit-event/edit-event.component';
import {EventsMapComponent} from '../components/events-map/events-map.component';

const appRoutes: Routes = [
  {path: 'event/:id', component: EventComponent},
  {path: 'events', component: EventsComponent},
  {path: 'map', component: EventsMapComponent},
  {path: 'new-event', component: NewEventComponent},
  {path: 'edit-event/:id', component: EditEventComponent},
  {path: 'user/:username', component: ProfileComponent},
  {path: 'settings', component: SettingsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: '', redirectTo: '/events', pathMatch: 'full'}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'enabled' })
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule {
}
