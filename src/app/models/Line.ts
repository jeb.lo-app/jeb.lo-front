export class Line {
  public readonly lineName: string;
  public readonly end1: string;
  public readonly end2: string;

  constructor(response: LineResponse) {
    this.lineName = response.lineId;
    this.end1 = response.end1;
    this.end2 = response.end2;
  }
}

export interface LineResponse {
  readonly lineId: string;
  readonly end1: string;
  readonly end2: string;
}
