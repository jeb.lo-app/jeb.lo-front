import {LatLng} from 'leaflet';

export class Stop {
  readonly id: number;
  readonly code: number;
  readonly name: string;
  readonly latitude: number;
  readonly longitude: number;

  constructor(response: StopResponse) {
    this.id = response.stopId;
    this.code = response.stopCode;
    this.name = response.stopName;
    this.latitude = response.stopLat;
    this.longitude = response.stopLon;
  }

  get coordinates(): LatLng {
    return {lat: this.latitude, lng: this.longitude};
  }
}

export interface StopResponse {
  readonly stopId: number;
  readonly stopCode: number;
  readonly stopName: string;
  readonly stopLat: number;
  readonly stopLon: number;
}
