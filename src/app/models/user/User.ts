export class User {
  public readonly photoSourceURL: string;
  public readonly username: string;
  public readonly joinedDate: Date;
  public readonly numberOfPosts: number;
  public readonly numberOfComments: number;
  public readonly numberOfUpvotes: number;

  constructor(inputDTO: UserResponse) {
    this.photoSourceURL = inputDTO.photoSource;
    this.username = inputDTO.username;
    this.joinedDate = new Date(inputDTO.joinedDate);
    this.numberOfPosts = inputDTO.numberOfPosts;
    this.numberOfComments = inputDTO.numberOfComments;
    this.numberOfUpvotes = inputDTO.likes;
  }

  public get joinedDateAsString(): string {
    return this.joinedDate.toLocaleDateString();
  }
}

export interface UserResponse {
  readonly photoSource: string;
  readonly username: string;
  readonly joinedDate: string;
  readonly numberOfPosts: number;
  readonly numberOfComments: number;
  readonly likes: number;
}
