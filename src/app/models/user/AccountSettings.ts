import {
  SubscriptionSettings,
  SubscriptionSettingsResponse,
  SubscriptionSettingsUpdateRequest
} from './SubscriptionSettings';

export class AccountSettings {
  public readonly avatarPhotoURL: string;
  public subscribesToNotifications: boolean;
  public readonly subscriptionSettings: SubscriptionSettings;

  constructor(response: AccountSettingsResponse) {
    this.avatarPhotoURL = response.photo;
    this.subscribesToNotifications = response.isSubscribe;
    this.subscriptionSettings = new SubscriptionSettings(response.subscriptionSettings);
  }

  toUpdateRequest(): AccountSettingsUpdateRequest {
    return {
      photo: this.avatarPhotoURL,
      isSubscribe: this.subscribesToNotifications,
      subscriptionSettings: this.subscriptionSettings.toUpdateRequest()
    };
  }
}

export interface AccountSettingsResponse {
  readonly isSubscribe: boolean;
  readonly photo: string;
  readonly subscriptionSettings: SubscriptionSettingsResponse;
}

export interface AccountSettingsUpdateRequest {
  readonly isSubscribe: boolean;
  readonly photo: string;
  readonly subscriptionSettings: SubscriptionSettingsUpdateRequest;
}
