export class SubscriptionSettings {
  public lineNames: string[];
  public stopNames: string[];

  constructor(response: SubscriptionSettingsResponse) {
    this.lineNames = response.lineNames;
    this.stopNames = response.stopNames;
  }

  public toUpdateRequest(): SubscriptionSettingsUpdateRequest {
    return {
      lineNames: this.lineNames,
      stopNames: this.stopNames
    };
  }
}

export interface SubscriptionSettingsResponse {
  readonly lineNames: string[];
  readonly stopNames: string[];
}

export interface SubscriptionSettingsUpdateRequest {
  readonly lineNames: string[];
  readonly stopNames: string[];
}
