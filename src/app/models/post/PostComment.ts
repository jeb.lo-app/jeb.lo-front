export class PostComment {
  public readonly authorPhoto: string;
  public readonly authorUsername: string;
  public readonly commentedDate: Date;
  public readonly text: string;
}

export interface PostCommentAddRequest {
  postId: number;
  text: string;
}
