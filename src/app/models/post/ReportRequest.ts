export interface ReportRequest {
  reason: string;
  content: string;
}
