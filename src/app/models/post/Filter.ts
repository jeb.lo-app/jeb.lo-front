export class Filter {
  get label(): string {
    return this.intLabel;
  }

  get filterBy(): string {
    return this.intFilterBy;
  }

  get filterValue(): string {
    return this.intFilterValue;
  }

  get enabled(): boolean {
    return this.intEnabled;
  }

  constructor(label: string, filterBy: string, filterValue: string, enabled: boolean) {
    this.intLabel = label;
    this.intFilterBy = filterBy;
    this.intFilterValue = filterValue;
    this.intEnabled = enabled;
  }

  private readonly intLabel: string;
  private readonly intFilterBy: string;
  private readonly intFilterValue: string;
  private intEnabled: boolean;

  public toggleEnabled(): void {
    this.intEnabled = !this.intEnabled;
  }
}
