import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import LatLng from 'leaflet';
import {PostComment} from './PostComment';

export class Post {
  public static readonly MAP_BOX_LATITUDE_RANGE = 0.05;
  public static readonly MAP_BOX_LONGITUTDE_RANGE = 0.08;

  public readonly id: number;
  public readonly title: string;
  public readonly postStatus: string;
  public readonly linesBlocked: string[];
  public readonly stopName: string;
  public readonly postedDate: Date;
  public readonly authorUsername: string;
  public readonly description: string;
  public readonly comments: PostComment[];
  readonly numberOfComments: number;
  private intUserHasVoted?: boolean;
  private intLikes: number;
  private intDislikes: number;
  public readonly coordinates: LatLng;
  public readonly photoSourceURLs: string[];

  constructor(response: PostResponse) {
    this.id = response.id;
    this.title = response.title;
    this.postStatus = response.statusPost;
    this.linesBlocked = response.linesBlocked;
    this.stopName = response.stopName;
    // this might be prone to errors!
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#Examples
    this.postedDate = new Date(response.postedDate);
    this.authorUsername = response.authorUsername;
    this.description = response.description;
    this.comments = response.comments;
    this.numberOfComments = response.numberOfComments;
    this.intUserHasVoted = response.isLike === null ? undefined : response.isLike;
    this.intLikes = response.likes;
    this.intDislikes = response.dislikes;
    this.coordinates = {lat: response.latitude, lng: response.longitude};
    this.photoSourceURLs = response.urlPhotos;
  }

  public getSafePhotoURL(domSanitizer: DomSanitizer): SafeResourceUrl[] {
    return this.photoSourceURLs.map(url => domSanitizer.bypassSecurityTrustUrl(url));
  }

  public get userHasVoted(): boolean | undefined {
    return this.intUserHasVoted;
  }

  public get likes(): number {
    return this.intLikes;
  }

  public get dislikes(): number {
    return this.intDislikes;
  }

  public like() {
    if (this.intUserHasVoted === undefined) {
      this.intUserHasVoted = true;
      this.intLikes += 1;
    } else if (this.intUserHasVoted === false) {
      this.intUserHasVoted = true;
      this.intDislikes -= 1;
      this.intLikes += 1;
    }
  }

  public eraseVote() {
    if (this.intUserHasVoted === true) {
      this.intUserHasVoted = undefined;
      this.intLikes -= 1;
    }

    if (this.intUserHasVoted === false) {
      this.intUserHasVoted = undefined;
      this.intDislikes -= 1;
    }
  }

  public dislike() {
    if (this.intUserHasVoted === undefined) {
      this.intUserHasVoted = false;
      this.intDislikes += 1;
    } else if (this.intUserHasVoted === true) {
      this.intUserHasVoted = false;
      this.intLikes -= 1;
      this.intDislikes += 1;
    }
  }
}

export interface PostResponse {
  readonly id: number;
  readonly title: string;
  readonly statusPost: string;
  readonly linesBlocked: string[];
  readonly stopName?: string;
  readonly postedDate: string;
  readonly authorUsername: string;
  readonly description?: string;
  readonly comments?: PostComment[];
  readonly likes: number;
  readonly dislikes: number;
  readonly urlPhotos: string[];
  readonly numberOfComments: number;
  readonly latitude: number;
  readonly longitude: number;
  readonly isLike?: boolean;
}

export interface PostAddRequest {
  title: string;
  lineNames: string[];
  description: string;
  latitude: number;
  longitude: number;
  photos: string[];
}

export interface PostEditRequest {
  title: string;
  lineNames: string[];
  description: string;
  latitude: number;
  longitude: number;
  photos: string[];
}
