import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {RoutingModule} from './routing/routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from './material/material.module';
import {EventModule} from './components/event/event.module';
import {EventsModule} from './components/events/events.module';
import {NewEventModule} from './components/new-event/new-event.module';
import {BottomNavigationComponent} from './components/bottom-navigation/bottom-navigation.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {SnackbarService} from './services/snackbar/snackbar.service';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {authInterceptorProviders} from './routing/AuthInterceptor';
import {ProfileModule} from './components/profile/profile.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {SettingsModule} from './components/settings/settings.module';
import {MatDialogModule} from '@angular/material/dialog';
import {EditEventComponent} from './components/edit-event/edit-event.component';
import {MatStepperModule} from '@angular/material/stepper';
import {SharedModule} from './components/shared/shared.module';
import {AngularFireModule} from '@angular/fire';
import {BUCKET} from '@angular/fire/storage';
import {AngularFireMessagingModule} from '@angular/fire/messaging';
import {MatCarouselModule} from '@ngmodule/material-carousel';
import {CookieService} from 'ngx-cookie-service';
import {EventsMapComponent} from './components/events-map/events-map.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';

@NgModule({
  declarations: [
    AppComponent,
    BottomNavigationComponent,
    LoginComponent,
    RegisterComponent,
    EditEventComponent,
    EventsMapComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        LayoutModule,
        RoutingModule,
        MaterialModule,
        FormsModule,
        EventModule,
        EventsModule,
        NewEventModule,
        ProfileModule,
        ReactiveFormsModule,
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        SettingsModule,
        HttpClientModule,
        MatDialogModule,
        MatStepperModule,
        SharedModule,
        AngularFireModule,
        AngularFireModule.initializeApp(environment.firebaseConfig, 'cloud'),
        MatCarouselModule.forRoot(),
        AngularFireMessagingModule,
        MatButtonToggleModule
    ],

  providers: [CookieService, SnackbarService, HttpClient, authInterceptorProviders,
    {
      provide: BUCKET,
      useValue: 'jeblo-storage.appspot.com'
    }],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
