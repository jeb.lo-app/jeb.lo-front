import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {map, shareReplay} from 'rxjs/operators';
import {TokenStorageService} from './services/token-storage/token-storage.service';
import {NavigationEnd, Router} from '@angular/router';
import {SnackbarService} from './services/snackbar/snackbar.service';
import * as firebase from 'firebase';
import {environment} from '../environments/environment';
import {AccountService} from './services/api/account/account.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'jeb.lo';
  username: string;
  currentLocation: string;
  isHomepage: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private tokenStorageService: TokenStorageService,
              private router: Router,
              private snackbar: SnackbarService,
              private accountService: AccountService,
              private location: Location) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentLocation = event.urlAfterRedirects;
        this.isHomepage = !(this.currentLocation !== '/events' && this.currentLocation !== '/map');
      }
    });
  }

  ngOnInit() {
    this.username = this.tokenStorageService.getUsername();
    if (this.userIsLoggedIn()) {
      this.snackbar.openSuccessSnackbar(`Witaj, ${this.username}!`);
      this.initializeMessaging();
    }
  }

  logout() {
    this.tokenStorageService.signOut();
    this.username = undefined;
    this.snackbar.openSuccessSnackbar('Wylogowano!');
    this.router.navigateByUrl('/');
  }

  goBack() {
    this.location.back();
  }

  userIsLoggedIn() {
    return this.tokenStorageService.userIsLoggedIn();
  }

  private initializeMessaging() {
    firebase.initializeApp(environment.firebaseConfig);

    const messaging = firebase.messaging();
    messaging.usePublicVapidKey('BCTFB4SPgGbssDl8b2TbuJb5G-E06ggjxdpxsUD2G7MpsKvPtIUbGhf8e0MqSKlqkMjINk6FaIdqsSb_bonbx-8');
    messaging.onTokenRefresh(() => {
      messaging.getToken().then((refreshedToken) => {
        console.log('Token refreshed.');
        // Send Instance ID token to app server.
        this.accountService.sendFCMToken(refreshedToken);
      }).catch((err) => {
        console.log('Unable to retrieve refreshed token ', err);
      });
    });

    messaging.onMessage(_ => {
      this.snackbar.openSuccessSnackbar('Nowe zdarzenie na subskrybowanych liniach! Odśwież aplikację, by je zobaczyć.');
    });
  }
}
