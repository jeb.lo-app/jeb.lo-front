//Install express server
const express = require('express');
const path = require('path');

const app = express();

// TODO: how to extract name of the application to a properties file or sth?
// Serve only the static files form the dist directory
app.use(express.static(path.join(__dirname, '/dist/jeb-lo-klata')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/jeb-lo-klata/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8081);

console.log('Node.js server has been started.');
console.log('Available under 127.0.0.1:8081 or 192.168.0.1:8081');
